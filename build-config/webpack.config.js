const path = require('path');

const VALID_ENVS = [ 'dev', 'inte', 'prod' ];

require('dotenv').config()

const {ENV} = process.env;
if (!ENV || VALID_ENVS.includes(ENV) !== true) {
  console.error(`Invalid ENV (${ENV}) Supported values are: ${VALID_ENVS.join(', ')}`);
  process.exit(1);
}

const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const FaviconsWebpackPlugin = require('favicons-webpack-plugin')

module.exports = (argv, env) => ({
  entry: {
    login   : './src/js/index-login.js',
    help    : './src/js/index-help.js',
    map     : './src/js/index-map.js',
    versions: './src/js/index-versions.js',
  },
  output: {
    path: path.resolve(__dirname, '../dist'),
  },
  mode: env.mode,
  devtool: env.mode === 'development' ? 'inline-source-map' : false,
  resolve: {
    alias: {
        'ui'     : path.resolve(__dirname, '../src/js/ui'),
        modules  : path.resolve(__dirname, '../src/js/modules'),
        lib      : path.resolve(__dirname, '../src/js/lib'),
        templates: path.resolve(__dirname, '../src/templates'),
        style    : path.resolve(__dirname, '../src/style'),
        assets   : path.resolve(__dirname, '../src/assets'),
        config   : path.resolve(__dirname, '../src/js/config.'+ENV+'.js'),
    }
  },
  module: {
    rules: [
      // style
      {
        test: /\.scss$/,
        use: [
          { loader: 'style-loader' },
          { loader: 'css-loader' },
          { loader: 'sass-loader' },
          { loader: 'postcss-loader',
            options: {
              postcssOptions: {
                plugins: [
                  'postcss-preset-env', // also includes 'autoprefixer'
                  'postcss-flexbugs-fixes',
                ]
              }
            }
          },
        ],
      },
      {
        test: /\.css$/,
        use: [
          { loader: 'style-loader' },
          { loader: 'css-loader' },
          { loader: 'postcss-loader',
            options: {
              postcssOptions: {
                plugins: [
                  'postcss-preset-env', // also includes 'autoprefixer'
                  'postcss-flexbugs-fixes',
                ]
              }
            }
          },
        ],
      },
      {
        test: /\.(png|svg|jpg|jpeg|gif)$/i,
        type: 'asset/resource',
      },
      // templates
      {
        test: /templates.*\.html$/,
        use: [
          'mustache-loader',
        ],
      },
      // markdown
      {
        test: /\.md/,
        use: [
          {
            loader: 'html-loader'
          },
          {
            loader: 'markdown-loader',
            options: {}
          }
        ]
      },
    ],
  },
  plugins: [
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({
      filename: 'help.html',
      template: 'public/help.html',
      chunks: ['help', 'runtime~help']
    }),
    new HtmlWebpackPlugin({
      filename: 'login.html',
      template: 'public/login.html',
      chunks: ['login', 'runtime~login']
    }),
    new HtmlWebpackPlugin({
      filename: 'map.html',
      template: 'public/map.html',
      chunks: ['map', 'runtime~map']
    }),
    new HtmlWebpackPlugin({
      filename: 'versions.html',
      template: 'public/versions.html',
      chunks: ['versions', 'runtime~versions']
    }),
    new FaviconsWebpackPlugin('./src/assets/img/factory.8.png'),
    // new BundleAnalyzerPlugin(),
  ],

});