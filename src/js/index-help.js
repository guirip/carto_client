import { executeOnDomReady } from './modules/Utils';
import { init } from './views/Help';

console.info('About to init help view');

executeOnDomReady(init);
