
module.exports = {
  fetchTimeout: 10000,
  staticPath: '/carto-static/',

  // disabled because user position is not updated often enough to guarantee a good UX
  geolocShowHeading: false,

  photo: {
    featureEnabled: false,
    photoDir: '/photos',
    diaposDir: '/diapos',
    thumbsDir: '/thumbs',
  },
};