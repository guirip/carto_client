'use strict';

const prodConfig = {
  backendPath: `/backend`, // apache proxy config
  wsUrl: `https://carto.heytonton.fr:3001`,
};

module.exports = require('deepmerge')(
  require('./config.common'),
  prodConfig
);
