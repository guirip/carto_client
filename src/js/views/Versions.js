import readme from '../../../README.md';

import 'style/versions-view.scss';

export const init = function () {
  document.querySelector('#container').innerHTML = readme;
};
