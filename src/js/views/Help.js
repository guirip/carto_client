
import {tplToDom} from 'modules/Utils';
import helpTpl from 'templates/help.html';

import 'style/common.scss';
import 'style/help-view.scss';

/**
 * Help view
 */

// Local variables
let container;

/**
 * Module templates
 */
const tplData = {
  pageTitle: 'Aide',
  filtreLabel: 'Filtre',
  ficheLabel: "Fiche d'un lieu",
  moveLabel: "Déplacement d'un lieu",
  addLabel: "Ajout d'un lieu",
  diaryLabel: 'Journal',
  searchLabel: 'Recherche',
  variousLabel: 'Divers',
};

/**
 * Initialise module
 */
export const init = function () {
  // Apply template
  container = tplToDom(helpTpl, tplData);

  // Add summary click handler
  const lis = container.querySelectorAll('li');
  for (const li of lis) {
    li.addEventListener('click', summaryClickHandler);
  }

  document.body.appendChild(container);
};

/**
 * Handles click on a summary element
 */
const summaryClickHandler = function ({ target }) {
  window.scrollTo({
    top: document.getElementById(target.dataset.target).offsetTop - 10,
    behavior: 'smooth'
  });
};
