
import * as ServiceHelper from 'modules/ServiceHelper';
import { fadeIn, getQueryParameter, addCssClassIfMobile } from 'modules/Utils';

import * as Messages from 'ui/Messages';
import { LINK_PARAM_NAME } from 'ui/SiteLink';

import 'style/common.scss';
import 'style/login-view.scss';

/**
 * Login page
 */

let
  container,
  ident,
  pass,
  btnValider;

const paramUser = getQueryParameter('user');

/**
 * Initialize module
 */
export const init = function () {
  addCssClassIfMobile();

  container = document.getElementById('login-container');

  ident = container.querySelector('#ident');
  ident.addEventListener('keyup', onKeyup);

  pass = container.querySelector('#pass');
  pass.addEventListener('keyup', onKeyup);

  btnValider = container.querySelector('#btn-valider');
  btnValider.addEventListener('click', valider);

  show();
};

/**
 * Display login form
 */
 const show = function () {
  fadeIn(container);

  // Feed user name field if given as parameter, then focus on the first empty input
  if (!paramUser) {
    ident.focus();
  } else {
    ident.value = paramUser;
    pass.focus();
  }
};

/**
 * Keyboard event handlers to validate when enter key is pressed
 * @param event
 */
 const onKeyup = function (event) {
  if (event.keyCode == '13') {
    valider();
  }
};

/**
 * Contrôle la saisie de l'utilisateur
 */
async function valider() {
  const
    identValue = ident.value.trim(),
    passValue = pass.value.trim();

  if (!identValue || !passValue) {
    Messages.warn('Saisie incomplète');
  }
  else {
    const { status, error } = await ServiceHelper.login(
      encodeURI(identValue),
      encodeURI(passValue)
    );
    if (error) return

    if (status === 200) {
      // Redirect to map
      const linkedId = getQueryParameter(LINK_PARAM_NAME);
      window.location = 'map.html' + (
        linkedId
          ? `?${LINK_PARAM_NAME}=${linkedId}`
          : ''
      );
    }
    else if (status === 401) {
      Messages.warn('Authentification en échec face de dindon !');
    }
  }
}
