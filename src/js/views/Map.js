import PubSub from 'pubsub-js';
import debounce from 'just-debounce';

import config from 'config';
import * as BroadcastHelper from 'modules/BroadcastHelper';
import * as Context from 'modules/Context';
import * as Events from 'modules/Events';
import * as GeoLocation from 'modules/GeoLocation';
import { load as loadMap } from 'modules/MapLoader';
import * as PushService from 'modules/PushService';
import * as ServiceHelper from 'modules/ServiceHelper';
import * as Utils from 'modules/Utils';

import { events as DiaryEvents } from 'ui/Diary';
import * as DragHandler from 'ui/DragHandler';
import { events as FicheEvents } from 'ui/Fiche';
import * as MapLongPress from 'ui/MapLongPress';
import { LINK_PARAM_NAME } from 'ui/SiteLink';

import {
  iconsPerEtat,
  selectedIconsPerEtat,
  userIcon,
  getUserIconWithCourse,
} from 'ui/MapIcons';

import 'style/common.scss';
import 'style/map-view.scss';

/**
 * Map module
 */

// Local variables
const
  mapContainerId = 'map-container',
  panoContainerId = 'pano-container',
  // Input id parameter
  linkedId = Utils.getQueryParameter(LINK_PARAM_NAME),
  // Map
  SITE_ZOOM = 18,
  DEFAULT_ZOOM = 11,
  DEPT_ZOOM = 10,
  DEFAULT_LAT = 48.800087,
  DEFAULT_LNG = 2.380489,
  DEFAULT_ZINDEX = 1,
  SELECTED_ZINDEX = 2,
  ZOOM_AFTER_PAN_TO_DELAY = 900;

let
  diagOkSubscription,
  mapContainer,
  panoContainer,
  btnBarEl,
  gpsBtn,
  map,
  panorama,
  userMarker,
  markers = {};

const currentFilterSelection = {};

/**
 * Check user session
 */
async function checkSession() {
  // server connection ok
  PubSub.unsubscribe(diagOkSubscription);

  // Check session
  const { data, error } = await ServiceHelper.checkSession();
  if (error) return;

  console.log('Auth OK. Welcome ' + data.user);
  PubSub.publish(Events.AUTH_SUCCESS);

  Context.setAuthor(data.user);

  // Init web socket connection
  PushService.init();

  // Check presence of input site id
  if (!checkLinkEntry()) {
    PubSub.publish((await import('ui/Filter')).events.SHOW);
  }
};

/**
 * Initialization
 */
export const init = async function() {
  Utils.addCssClassIfMobile();

  //Utils.clearLocalStorage();

  mapContainer = document.getElementById(mapContainerId);
  panoContainer = document.getElementById(panoContainerId);

  window.onunload = function() {
    // Fermeture tooltip à la fermeture de la fenêtre
    if (Context.getCurrentFicheId()) {
      BroadcastHelper.broadcastFicheClosed(
        Context.getCurrentFicheId(),
        Context.getAuthor()
      );
    }

    // Clear context data
    //Utils.clearLocalStorage();

    // Close WS connection
    PushService.quit();
  };

  // Diagnose server status
  const Diag = await import('ui/Diag');
  diagOkSubscription = PubSub.subscribe(Diag.events.IS_OK, checkSession);

  btnBarEl = document.getElementById('btn-bar');

  // Add button bar event listeners
  const diaryBtn = btnBarEl.querySelector('#btn-bar-diary');
  if (diaryBtn) {
    diaryBtn.addEventListener('click', async function() {
      PubSub.publish(DiaryEvents.SHOW);
    });
  }

  const filterBtn = btnBarEl.querySelector('#btn-bar-filter');
  if (filterBtn) {
    filterBtn.addEventListener('click', async function() {
      PubSub.publish((await import('ui/Filter')).events.SHOW);
    });
  }

  gpsBtn = btnBarEl.querySelector('#btn-bar-geolocation');
  if (gpsBtn) {
    if (location.protocol !== 'https:') {
      gpsBtn.style.display = 'none';
    } else {
      gpsBtn.addEventListener('click', toggleGeoLocation);
    }
  }

  const helpBtn = btnBarEl.querySelector('#btn-bar-help');
  if (helpBtn) {
    helpBtn.addEventListener('click', function() {
      window.open('help.html');
    });
  }

  const logoutBtn = btnBarEl.querySelector('#btn-bar-logout');
  if (logoutBtn) {
    logoutBtn.addEventListener('click', async function() {
      const Messages = await import('ui/Messages');

      // Ask for confirmation before proceeding to logout
      Messages.confirm(
        'Confirmes-tu la déconnexion ?',
        async function() {
          const { result } = await ServiceHelper.logout();
          if (result === true) {
            Messages.info('Tu as bien été déconnecté.');
            setTimeout(Utils.goLogin, 2000);
          }
        },
        null,
        'topRight'
      );
    });
  }

  loadMap();
  import('ui/DeptMap');
  import('ui/Recherche');
};

/**
 * Callback when Maps API is ready
 */
const onGoogleMapsAPIInitialized = function() {
  console.log('Google Maps API successfully loaded.');

  // Init streetview
  panorama = new google.maps.StreetViewPanorama(
    panoContainer,
    {
      enableCloseButton: true,
      visible: false,
      fullscreenControl: false,
    }
  );
  panorama.addListener(
    'visible_changed',
    panoVisibleChanged
  );
  panorama.addListener('closeclick', panoCloseClickHandler);

  panorama.addListener('position_changed', panoMoveHander);

  // Create map instance
  map = new google.maps.Map(document.getElementById(mapContainerId), {
    zoom: DEFAULT_ZOOM,
    center: new google.maps.LatLng(DEFAULT_LAT, DEFAULT_LNG),
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    scaleControl: true, // affichage de l'échelle
    fullscreenControl: false, // due to UI issue
    streetView: panorama,
  });

  map.addListener('rightclick', function(e) {
    mapRightClickHandler(e.latLng);
  });
  map.addListener('mousedown', MapLongPress.start);
  map.addListener('mouseup', MapLongPress.cancel);
  map.addListener('bounds_changed', MapLongPress.cancel);

  resize();
  console.log('Map initialized');

  // Show button bar
  const buttonBarEl = document.getElementById('btn-bar');
  if (buttonBarEl) {
    buttonBarEl.style.display = 'flex';
  }

  PubSub.publish(Events.MAP_READY);
};
window.onGoogleMapsAPIInitialized = onGoogleMapsAPIInitialized;

const checkLinkEntry = function() {
  if (linkedId) {
    // Large defer
    window.setTimeout(async function() {
      const { data:site, error } = await ServiceHelper.getSite(linkedId);
      if (error) return; // TODO: show message?

      createMarker(site);
      showFiche(linkedId);

      // Zoom on site when fiche is ready
      const subscription = PubSub.subscribe(FicheEvents.OPEN, function() {
        PubSub.unsubscribe(subscription);
        window.setTimeout(zoomOnCurrentSite, 700);
        window.setTimeout(function() {
          map.setMapTypeId(google.maps.MapTypeId.HYBRID);
        }, 2800);
      });
    }, 500);

    return true;
  } else {
    return false;
  }
};

/**
 * Adapt map size to window size
 */
const resize = function() {
  google.maps.event.trigger(map, 'resize');
};
PubSub.subscribe(Events.WINDOW_RESIZE, resize);


// USER GEOLOCATION

const GPS_BTN_STATUS_CLASS = 'gps-on';
const GPS_BTN_STATUS_ONGOING_CLASS = 'blink';
let gpsEnabled = false;

function toggleGeoLocation() {
  if (gpsEnabled) {
    disableGeolocation();
  } else {
    enableGeolocation();
  }
}

function disableGeolocation() {
  gpsEnabled = false;
  gpsBtn.classList.remove(GPS_BTN_STATUS_ONGOING_CLASS);
  gpsBtn.classList.remove(GPS_BTN_STATUS_CLASS);
  GeoLocation.stop();
  userMarker.setVisible(false);
  userMarker.setPosition(null);
}

function enableGeolocation() {
  gpsEnabled = true;
  gpsBtn.classList.add(GPS_BTN_STATUS_ONGOING_CLASS);
  GeoLocation.start();
}

/**
 * Handles new user position
 * @param  {string} eventName
 * @param  {object} pos
 * @doc https://gengns.com/rotate-markers-in-google-maps/
 */
function onNewUserPosition(eventName, pos) {
  gpsBtn.classList.remove(GPS_BTN_STATUS_ONGOING_CLASS);
  gpsBtn.classList.add(GPS_BTN_STATUS_CLASS);

  const markerWasNotVisible = !userMarker || userMarker.visible !== true;

  let icon;
  if (config.geolocShowHeading) {
    icon = `data:image/svg+xml;charset=utf-8,${encodeURIComponent(getUserIconWithCourse(pos.heading))}`;
  } else {
    icon = userIcon;
  }

  if (!userMarker) {
    // Create it
    userMarker = new google.maps.Marker({
      map,
      position: pos,
      zIndex: DEFAULT_ZINDEX,
      icon,
    });
  } else {
    // Update the instance
    if (config.geolocShowHeading) {
      userMarker.setIcon(icon);
    }
    userMarker.setPosition(pos);
    userMarker.setVisible(true);
  }

  if (markerWasNotVisible) {
    map.panTo(pos);
  }
}
PubSub.subscribe(Events.USER_LOCATION, onNewUserPosition);

/**
 * Handle user position error
 * @param  {string} eventName
 * @param  {object} error
 */
async function onUserPositionError(eventName, error) {
  (await import('ui/Messages')).warn('Erreur de géolocalisation: '+error?.message);
  disableGeolocation();
}
PubSub.subscribe(Events.USER_LOCATION_ERROR, onUserPositionError);


// MARKERS INITIALIZATION

/**
 * Get sites
 * @param event
 * @param args
 */
async function fetchSites(event, { depts, etats }) {
  currentFilterSelection.depts = depts;
  currentFilterSelection.etats = etats;

  const { data:sites, error } = await ServiceHelper.getSites(depts, etats);
  if (error) return;

  dataFetched(sites);
}
PubSub.subscribe(Events.GET_SITES, fetchSites);

/**
 * Fetch and display sites around a position
 * @param  {object} event
 * @param  {object} parameters such as : { lat, lng, etats, distance, focus }
 */
async function fetchSitesAround(event, { lat, lng, etats, distance, focus }) {
  const { data:sites, error } = await ServiceHelper.getSitesAround(
    lng,
    lat,
    distance,
    etats
  );
  if (error) return;

  dataFetched(sites);
}
PubSub.subscribe(Events.GET_SITES_AROUND, fetchSitesAround);


function siteMatchCurrentFilter(site) {
  return currentFilterSelection
      && Array.isArray(currentFilterSelection.etats) && currentFilterSelection.etats.indexOf(site.ETAT) !== -1
      && Array.isArray(currentFilterSelection.depts) && currentFilterSelection.depts.indexOf(site.DEPARTEMENT || site.PAYS) !== -1;
}

/**
 * @param  {String} event
 * @param  {array} data
 */
const handleRefreshSitesEvent = function(event, data) {
  dataFetched(data);
};
PubSub.subscribe(Events.REFRESH_SITES, handleRefreshSitesEvent);

/**
 * Refresh map content
 * @param {array} sites
 */
const dataFetched = async function(sites) {
  PubSub.publish((await import('ui/Filter')).events.HIDE);
  PubSub.publish((await import('ui/Around')).events.HIDE);

  // Remove currently displayed sites
  for (const siteId in markers) {
    markers[siteId].setMap(null);

    // Remove tooltip
    PubSub.publish((await import('ui/MapTooltips')).events.REMOVE, siteId);

    if (Context.getCurrentFicheId()) {
      PubSub.publish(FicheEvents.CLOSE);
    }
  }
  markers = {};

  // Dev
  if (Utils.isDevEnv()) {
    window.markers = markers;
  }

  // Add markers set
  let siteCount = 0;
  if (sites) {
    sites.forEach((site) => {
      createMarker(site);
      siteCount++;
    });
  }
  console.log(siteCount + ' markers loaded.');

  // Get users current fiche id
  BroadcastHelper.broadcastWhereAreYou(Context.getAuthor());
};

/**
 * Add a site to map
 * @param site
 */
const createMarker = function(site) {
  markers[site._id] = new google.maps.Marker({
    map: map,
    position: new google.maps.LatLng(site.GEO_POINT.coordinates[1], site.GEO_POINT.coordinates[0]),
    icon: {
      url: getIconName(site),
      size: new google.maps.Size(45, 45),
    },
    zIndex: DEFAULT_ZINDEX,
    shape: {
      type: 'circle',
      coords: [site.GEO_POINT.coordinates[0], site.GEO_POINT.coordinates[1], 45],
    }, // Reduce click area
    // custom attributes :
    id: site._id,
    etat: site.ETAT,
    nbUsers: 0,
  });
  google.maps.event.addListener(
    markers[site._id],
    'click',
    markerLeftClickHandler
  );
  google.maps.event.addListener(
    markers[site._id],
    'dragstart',
    DragHandler.markerDragstartHandler
  );
  google.maps.event.addListener(
    markers[site._id],
    'dragend',
    DragHandler.markerDragendHandler
  );
};

/**
 * @param site
 */
const getIconName = (site) => iconsPerEtat[site.ETAT];

/**
 * Add selection circle below a marker
 * @param id
 */
const selectMarker = function(id) {
  if (markers[id]) {
    markers[id].setIcon(selectedIconsPerEtat[markers[id].etat]);
    markers[id].setZIndex(SELECTED_ZINDEX);
  }
};

/**
 * Remove selection circle below a marker
 * @param id
 */
const unselectMarker = function(id) {
  if (markers[id]) {
    markers[id].setIcon(iconsPerEtat[markers[id].etat]);
    markers[id].setZIndex(DEFAULT_ZINDEX);
  }
  BroadcastHelper.broadcastFicheClosed(id, Context.getAuthor());
};

async function updateMarkerDraggableStatus(eventName, { draggable, siteId }) {
  // console.log('Site '+siteId+' new draggable status is: '+draggable);
  const marker = markers[siteId];
  if (!marker) {
    // Safety net
    return;
  }

  marker.setDraggable(draggable);

  if (draggable) {
    (await import('ui/Messages')).info('Le point peut maintenant être déplacé.');
  }
}
PubSub.subscribe(Events.MARKER_DRAGGABLE, updateMarkerDraggableStatus);

/**
 * Move a marker
 * @param event
 * @param data
 */
const moveMarkerHandler = function(event, data) {
  if (markers[data.id]) {
    markers[data.id].setPosition(data.position);
  }
};
PubSub.subscribe(Events.MARKER_MOVE, moveMarkerHandler);

// FICHE INFO

/**
 * Called when marker is left clicked
 */
const markerLeftClickHandler = function() {
  if (this.draggable) {
    // Skip fiche opening when drag feature is enabled
    return;
  }

  if (Context.getCurrentFicheId() == this.id) {
    // Close fiche
    PubSub.publish(FicheEvents.CLOSE);
  } else {
    showFiche(this.id);
  }
};

/**
 * Show the fiche for a site
 * @param id
 */
async function showFiche(id) {
  if (!id) {
    console.error('id not defined, cannot open fiche');
  } else {

    // Unselect currently selected (if different)
    if (Context.getCurrentFicheId() && Context.getCurrentFicheId() != id) {
      unselectMarker(Context.getCurrentFicheId());
    }
    // Cancel current drag (if different)
    if (Context.getCurrentDragId() && Context.getCurrentDragId() != id) {
      DragHandler.cancelDrag();
    }

    const { data:site, error } = await ServiceHelper.getSite(id);
    if (error) return;

    // Open fiche
    PubSub.publish(FicheEvents.OPEN, site);

    // Add site on map if absent (focus from diary for instance)
    if (!markers[site._id]) {
      createMarker(site);
    }

    // Change icon style
    selectMarker(site._id);

    if (site._id != Context.getCurrentFicheId()) {
      // Show tooltip
      BroadcastHelper.broadcastFicheOpened(site._id, Context.getAuthor());
    }

    Context.setCurrentFicheId(site._id);
  }
};

/**
 * Open current fiche
 */
const showCurrentFiche = function() {
  showFiche(Context.getCurrentFicheId());
};

/**
 * Close fiche
 */
const hideFiche = function() {
  if (Context.getCurrentFicheId()) {
    unselectMarker(Context.getCurrentFicheId());
  }
  Context.setCurrentFicheId('');
};
PubSub.subscribe(FicheEvents.CLOSE, hideFiche);


// SITES NAVIGATION (previous/next)

/**
 * Focus on the previous site
 * @param  {string} eventName
 * @param  {string} currentSiteId
 */
function goToPreviousSite(eventName, currentSiteId) {
  const sitesId = Object.keys(markers);
  const currentIndex = sitesId.findIndex(id => id === currentSiteId);

  let nextSiteId;
  if (currentIndex === 0) {
    // Go to last marker
    nextSiteId = sitesId[sitesId.length-1];
  } else {
    nextSiteId = sitesId[currentIndex-1];
  }
  showFiche(nextSiteId);
  map.panTo(markers[nextSiteId].position)
}
PubSub.subscribe(Events.MARKER_GO_TO_PREVIOUS, goToPreviousSite);

/**
 * Focus on the next site
 * @param  {string} eventName
 * @param  {string} currentSiteId
 */
function goToNextSite(eventName, currentSiteId) {
  const sitesId = Object.keys(markers);
  const currentIndex = sitesId.findIndex(id => id === currentSiteId);

  let nextSiteId;
  if (currentIndex === sitesId.length-1) {
    // Go to first marker
    nextSiteId = sitesId[0];
  } else {
    nextSiteId = sitesId[currentIndex+1];
  }
  showFiche(nextSiteId);
  map.panTo(markers[nextSiteId].position)
}
PubSub.subscribe(Events.MARKER_GO_TO_NEXT, goToNextSite);


// STREET VIEW

const MINIMAP_CLASS = 'minimap';
const PANO_DISPLAY = {
  visible: 'block',
  hidden: 'none',
};

const panoVisibleChanged = function() {
  if (panorama.getVisible()) {
    if (panoContainer.style.display !== PANO_DISPLAY.visible) {
      // Open streetview
      panoContainer.style.display = PANO_DISPLAY.visible;
      mapContainer.classList.add(MINIMAP_CLASS);
      btnBarEl.style.display = 'none';
      if (map.getZoom() < 14) {
        map.setZoom(16);
      }
      resize();
      PubSub.publish(Events.MAP_STREETV_OPEN);
    }
    window.setTimeout(delayedUpdatePositionFromPanorama, 200);
  }
};
const delayedUpdatePositionFromPanorama = function() {
  map.panTo(panorama.getPosition());
};

const panoCloseClickHandler = function() {
  panoContainer.style.display = PANO_DISPLAY.hidden;
  mapContainer.classList.remove(MINIMAP_CLASS);
  btnBarEl.style.display = 'flex';
  resize();
  PubSub.publish(Events.MAP_STREETV_CLOSE);
};

const panoMoveHander = function() {
  map.panTo(panorama.getPosition());
}

// MENU

/**
 * @param {object} latLng
 */
const mapRightClickHandler = async function(latLng) {
  PubSub.publish(FicheEvents.CLOSE);
  PubSub.publish((await import('ui/Filter')).events.HIDE);

  PubSub.publish((await import('ui/Menu')).events.SHOW, {
    latLng: latLng,
    map: map,
  });
};
PubSub.subscribe(Events.MAP_LONG_PRESS, function(_eventName, coords) {
  mapRightClickHandler(coords);
});

// ZOOMS / FOCUS

/**
 * Zoom on a site
 */
const zoomOnCurrentSite = function() {
  if (Context.getCurrentFicheId()) {
    zoomOnSite(Context.getCurrentFicheId());
  }
};
PubSub.subscribe(Events.SITE_ZOOM_CURRENT, zoomOnCurrentSite);
PubSub.subscribe(Events.SITE_ZOOM, async function(eventName, siteId) {
  if (!markers[siteId]) {
    const { data:site, error } = await ServiceHelper.getSite(siteId);
    if (error) return;
    createMarker(site);
  }
  zoomOnSite(siteId);
});

/**
 * @param {string} id
 */
const zoomOnSite = function(id) {
  if (markers[id]) {
    map.panTo(markers[id].position);
    if (map.zoom < SITE_ZOOM) {
      window.setTimeout(applyZoom, ZOOM_AFTER_PAN_TO_DELAY, SITE_ZOOM);
    }
  }
};

/**
 * Pan to a location
 * @param {string} eventName
 * @param {object} data
 */
const zoomOnLocation = function(eventName, { location, zoom }) {
  if (location) {
    applyZoom(zoom ?? DEPT_ZOOM);
    map.panTo(location);
  }
};
PubSub.subscribe(Events.MAP_PAN_TO, zoomOnLocation);

/**
 * @param {number} zoom
 */
const applyZoom = function(zoom) {
  map.setZoom(zoom);
};

PubSub.subscribe(Events.MAP_ZOOM_TO, function(eventName, zoomValue) {
  applyZoom(zoomValue);
});

/**
 * Focus on a site : zoom + fiche + tooltip
 * @param {string} eventName
 * @param {string} siteId
 */
const focusOnSite = async function(eventName, siteId) {
  if (!markers[siteId]) {
    const { data:site, error } = await ServiceHelper.getSite(siteId);
    if (error) return;
    createMarker(site);
  }
  showFiche(siteId);
  tryZoom(siteId, 400, 8);
};
PubSub.subscribe(Events.SITE_FOCUS, focusOnSite);

/**
 * Focus on site when it is loaded on map, trying each [interval] ms until [remaining]
 * @param {int} id
 * @param {int} interval
 * @param {int} remaining
 */
const tryZoom = function(id, interval, remaining) {
  if (markers[id]) {
    zoomOnSite(id);
  } else if (remaining > 0) {
    window.setTimeout(function() {
      tryZoom(id, interval, remaining - 1);
    }, interval);
  }
};

// TOOLTIPS

/**
 * Add a site tooltip to the map
 * @param {string} _event
 * @param {InfoBox} ttp
 */
const addSiteTooltipHandler = function(_event, ttp) {
  ttp.open({
    map,
    anchor: markers[ttp.id]
  });
};
PubSub.subscribe(Events.MAP_ADD_SITE_TOOLTIP, addSiteTooltipHandler);

/**
 * Add a tooltip to the map
 * @param {string} _event
 * @param {InfoBox} ttp
 */
const showTooltipHandler = function(_event, { ttp, position }) {
  ttp.setPosition(position);
  ttp.open({ map });
};
PubSub.subscribe(Events.MAP_SHOW_TOOLTIP, showTooltipHandler);


// WEB SOCKET EVENTS HANDLERS

/**
 * Handle SITE_CREATED event
 * @param eventName
 * @param event
 */
const siteCreatedHandler = function(eventName, event) {
  const userIsTheAuthor = event.author === Context.getAuthor();

  const site = event.data;

  if (siteMatchCurrentFilter(site) || userIsTheAuthor) {
    createMarker(site);

    if (userIsTheAuthor) {
      showFiche(site._id);
    }
    else {
      ficheOpened({
        data  : site._id,
        author: event.author,
      });
    }
  }
};
PubSub.subscribe(Events.SITE_CREATED, siteCreatedHandler);

/**
 * Handle SITE_POSITION_UPDATED event
 * @param eventName
 * @param event
 */
const sitePositionUpdatedHandler = function(eventName, event) {
  const site = event.data;

  // Affichage de la fiche avec les coordonnées actualisées
  if (Context.getCurrentFicheId() == site._id) {
    showCurrentFiche();
  }
  if (event.author != Context.getAuthor()) {
    // Update marker position
    markers[site._id].setPosition(
      new google.maps.LatLng(site.GEO_POINT.coordinates[1], site.GEO_POINT.coordinates[0])
    );
  }
};
PubSub.subscribe(
  Events.SITE_POSITION_UPDATED,
  sitePositionUpdatedHandler
);

/**
 * Handle SITE_ETAT_UPDATED event
 * @param eventName
 * @param event
 */
const siteEtatUpdatedHandler = function(eventName, event) {
  const site = event.data;

  if (markers[site._id]) {

    // Remove marker from map because it does not match filter criteria
    if (event.author !== Context.getAuthor() && siteMatchCurrentFilter(site) !== true) {
      markers[site._id].setMap(null);
      delete markers[site._id];
      return;
    }

    // Update icon name
    markers[site._id].etat = site.ETAT;
    markers[site._id].iconName = getIconName(site);

    if (Context.getCurrentFicheId() == site._id) {
      // Refresh fiche content
      showCurrentFiche();
      // Will also update the icon to indicate the currently selected site
    } else {
      // Update icon normal
      markers[site._id].setIcon(markers[site._id].iconName);
    }
  }
  else if (siteMatchCurrentFilter(site)) {
    createMarker(site);

    ficheOpened({
      data  : site._id,
      author: event.author,
    });
  }
};
PubSub.subscribe(
  Events.SITE_ETAT_UPDATED,
  siteEtatUpdatedHandler
);

/**
 * Handle REMARQUE_CREATED event
 * @param eventName
 * @param event
 */
const remarqueCreatedHandler = function(eventName, event) {
  if (event.data.SITE_ID == Context.getCurrentFicheId()) {
    showCurrentFiche();
  }
};
PubSub.subscribe(
  Events.REMARQUE_CREATED,
  remarqueCreatedHandler
);

/**
 * Handle PHOTO_ADDED event
 * @param event
 * @param data
 */
const photoAddedHandler = function(event, data) {
  if (data.data.SITE_ID == Context.getCurrentFicheId()) {
    debounce(200, showCurrentFiche)();
  }
};
PubSub.subscribe(Events.PHOTO_ADDED, photoAddedHandler);

/**
 * Handle WHERE_R_U event
 * @param event
 * @param data
 */
const whereAreYouHandler = function(event, data) {
  if (data.author !== Context.getAuthor() && Context.getCurrentFicheId()) {
    BroadcastHelper.broadcastFicheOpened(
      Context.getCurrentFicheId(),
      Context.getAuthor()
    );
  }
};
PubSub.subscribe(Events.WHERE_R_U, whereAreYouHandler);

/**
 * Handle FICHE_OPENED event
 * @param data
 */
const ficheOpened = async function(data) {
  if (markers[data.data]) {
    PubSub.publish((await import('ui/MapTooltips')).events.ADD, data);
  }
};
PubSub.subscribe(Events.FICHE_OPENED, function(eventName, data) {
  ficheOpened(data);
});


export const performInit = () => {
  if (document.readyState === 'complete') {
    init();
  } else {
    document.onreadystatechange = function() {
      if (document.readyState == 'complete') {
        init();
      }
    };
  }
};
