import { executeOnDomReady } from './modules/Utils';
import { init } from './views/Versions';

console.info('About to init versions view');

executeOnDomReady(init);
