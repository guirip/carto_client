import PubSub from 'pubsub-js';

import * as Events from './Events';
import * as Messages from 'ui/Messages';

/**
 * Utils module
 */

/**
 * Log something prepending date & time
 * @param string
 */
export const log = (string) => {
  if (console) {
    console.log('[' + getCurrentDateTimeLabel() + '] ' + string);
  }
};

/**
 * Log something prepending date & time
 * @param string
 */
export const logError = (string) => {
  if (console) {
    console.log('[' + getCurrentDateTimeLabel() + '] ' + string);
  }
};

/**
 * Return a current date & time label
 */
export const getCurrentDateTimeLabel = () => {
  return new Date().toISOString();
};

const isMobile = 'ontouchstart' in document.documentElement;
export const isMobileDevice = () => isMobile;

export function addCssClassIfMobile() {
  if (isMobileDevice()) {
    document.body.classList.add('mobile');
  }
}

export const isDevEnv = () =>
  location.hostname === 'localhost' || location.hostname.startsWith('192.168');

/**
 * @param {function} next
 */
let onReadyCallbacks = [];
export const executeOnDomReady = (next) => {
  if (typeof next !== 'function') {
    logError('[executeOnDomReady] callback is not a function', next);
  } else {
    if (document.readyState === 'complete') {
      next();
    } else {
      onReadyCallbacks.push(next);
    }
  }
};
document.onreadystatechange = () => {
  if (document.readyState == 'complete') {
    onReadyCallbacks.forEach((func) => {
      func();
    });
    onReadyCallbacks = [];
  }
};

const PX_REGEXP = /^(\d*)px$/;
function pxValueToNumber(value) {
  const result = PX_REGEXP.exec(value);
  const num = result?.[1];
  if (num) {
    return parseInt(num, 10);
  }
  return 0;
}

const SLIDE_DURATION = 350;

/**
 * Show a popup
 * @param el
 * @param { function } cb (optional)
 */
export const showPopup = (el, cb) => {
  const origTop = pxValueToNumber(el.style.top);
  el.style.opacity = 0;
  el.style.top = `${origTop-100}px`;
  el.style.transition = `top ${SLIDE_DURATION/1000}s, opacity ${SLIDE_DURATION/1000}s`;
  el.style.display = 'block';

  window.requestAnimationFrame(() => {
    el.style.top = `${origTop}px`;
    el.style.opacity = 1;

    if (typeof cb === 'function') {
      window.setTimeout(cb, SLIDE_DURATION);
    }
  });
};

/**
 * Hide a popup
 * @param el
 * @param { function } cb (optional)
 */
export const hidePopup = (el, cb) => {
  const top = pxValueToNumber(el.style.top);

  el.style.top = `${top-100}px`;
  el.style.opacity = 0;

  window.setTimeout(function() {
    el.style.display = 'none';
    el.style.top = `${top}px`;

    if (typeof cb === 'function') {
      cb();
    }
  }, SLIDE_DURATION);
};

const TRANSITION_STYLE = 'opacity .7s';
export function fadeIn(el) {
  // TODO: update style.display
  el.style.opacity = 0;
  el.style.transition = TRANSITION_STYLE;

  window.requestAnimationFrame(() => {
    el.style.opacity = 1;
  });
}
export function fadeOut(el) {
  el.style.opacity = 1;
  el.style.transition = TRANSITION_STYLE;

  window.requestAnimationFrame(() => {
    el.style.opacity = 0;

    // TODO: update style.display after 700ms
  });
}

/**
 * Get the value of a query parameter
 * @param name
 */
export const getQueryParameter = (name) => {
  name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
  const regex = new RegExp('[\\?&]' + name + '=([^&#]*)'),
    results = regex.exec(window.location.search);

  return !results ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
};

export const getNowLabel = () => {
  return formatDate(new Date());
};

export const formatDate = (date) => {
  if (!date) {
    return '';
  } else {
    let _date;
    if (typeof date === 'string' || typeof date === 'number') {
      _date = new Date(date);
    } else {
      _date = date;
    }
    return (
      to2Digits(_date.getDate()) +
      '/' +
      to2Digits(_date.getMonth() + 1) +
      '/' +
      _date.getFullYear().toString() +
      ' à ' +
      to2Digits(_date.getHours()) +
      'h' +
      to2Digits(_date.getMinutes())
    );
  }
};

/**
 * @param number
 */
export const to2Digits = (number) => {
  return number < 10 ? '0' + number : number;
};

/**
 * @param  {float} float
 * @return {float}
 *
 * e.g roundWgs(48.79142650000001)
 * returns 48.79143
 */
export const roundWgs = (float) => Math.round(float * 100000) / 100000;

/**
 * Empty localStorage data
 */
export const clearLocalStorage = () => {
  for (const key in window.localStorage) {
    window.localStorage.removeItem(key);
  }
};

/**
 * Simple sugar json parsing
 * @param data
 */
export const parseJSON = (data) => {
  try {
    return JSON.parse(data);
  } catch (e) {
    Messages.error('Flux de données invalide : ' + e);
  }
};

/**
 * Applies template content to a new div element
 * @param {function} fn
 * @param {object} data
 */
export const tplToDom = (fn, data) => {
  if (typeof fn !== 'function') {
    console.error('wrong argument received, expected a function, got: ', fn);
  }
  const tplString = fn(data);

  const div = document.createElement('div');
  div.innerHTML = tplString;
  return div.children[0];
};

/**
 * Simply add a dom node to body element
 * @param {HTML element} el (dom node)
 */
export const appendModule = el => {
  if (el) {
    document.body.appendChild(el);
  }
};

/**
 * Add a ui module to DOM and display it
 * @param el
 */
export function appendAndShowModule(el) {
  fadeIn(el);
  appendModule(el);
}

let FADE_OUT_DURATION = 400;
export const closeModule = el => {
  el.style.opacity = 1;
  el.style.transition = `opacity .${FADE_OUT_DURATION / 100}s`;
  el.style.opacity = 0;
  window.setTimeout(setDisplayNone, FADE_OUT_DURATION, el);
};
function setDisplayNone(el) {
  el.style.display = 'none';
}

let timerPublishResizeEvent;
/**
 * Propagate resize event
 */
window.onresize = function () {
  if (timerPublishResizeEvent) {
    window.clearTimeout(timerPublishResizeEvent);
  }
  timerPublishResizeEvent = window.setTimeout(publishResizeEvent, 500);
};
const publishResizeEvent = function () {
  PubSub.publish(Events.WINDOW_RESIZE);
};

/**
 * Check actions to perform on user click
 * @param e
 */
export const clickOutHandler = (e) => {
  PubSub.publish(Events.BODY_CLICKED, e.target);
};
executeOnDomReady(() => {
  // document.body.addEventListener('click', clickOutHandler);

  let downTime;

  document.body.addEventListener('mousedown', downHandler);
  document.body.addEventListener('pointerdown', downHandler);
  document.body.addEventListener('mouseup', upHandler);
  document.body.addEventListener('pointerup', upHandler);

  function downHandler() {
    downTime = new Date().getTime();
  }
  function upHandler(e) {
    if (new Date().getTime() - downTime < 500) {
      clickOutHandler(e);
    }
  }
});

/**
 * Redirect to login page
 * @param  {Array[{key:..., value:...},...]} params : url query parameters for login page
 */
export const goLogin = (params) => {
  let url = 'login.html';

  // Add query parameters
  if (params) {
    params.forEach((param, index) => {
      url = url
        .concat(index === 0 ? '?' : '&')
        .concat(param.key)
        .concat('=')
        .concat(param.value);
    });
  }
  window.location = url;
};

export const removeHtmlTags = (str='') => str.replace(/(<([^>]+)>)/gi, '');

/**
 * @param  {DOM element} element
 * @param  {function}    matcher
 * @return {DOM element} null if no match is found
 */
export function findParentNode(element, matcher) {
  if (!element || typeof element !== 'object') {
    return null;
  }
  if (typeof matcher !== 'function') {
    console.warn('invalid argument, expected matcher to be a function, got: ', matcher);
    return null;
  }

  if (matcher(element)) {
    return element;
  } else {
    return findParentNode(element.parentNode, matcher);
  }
};

export function findChildren(element, matcher) {
  if (!element || typeof element !== 'object') {
    return null;
  }
  if (typeof matcher !== 'function') {
    console.warn('invalid argument, expected matcher to be a function, got: ', matcher);
    return null;
  }

  const { childNodes } = element;
  const matches = [];
  childNodes.forEach(child => {
    if (child.nodeType !== Node.ELEMENT_NODE) {
      return;
    }
    if (matcher(child)) {
      matches.push(child);
    }
  });
  return matches;
}

export function toggleCssClass(el, className) {
  if (el.classList.contains(className)) {
    el.classList.remove(className);
  } else {
    el.classList.add(className);
  }
}

const getInfowindowCloseButton = (contentEl) => (
  contentEl.parentNode.parentNode.parentNode.querySelector('button.gm-ui-hover-effect')
)

// The infobox close icon is small and ugly
// let's mimic the style of the other popups
export function overrideInfowindowCloseIcon(contentEl) {
  const buttonEl = getInfowindowCloseButton(contentEl);
  if (!buttonEl) {
    log(`Cannot override infowindow close button because element wasn't found in container:`, containerEl);
    return;
  }
  buttonEl.innerHTML = '<span class="btn popup-close" title="Fermer"><i class="fas fa-times-circle"></i></span>';
}

export function removeInfowindowCloseIcon(contentEl) {
  const buttonEl = getInfowindowCloseButton(contentEl);
  if (!buttonEl) {
    log(`Cannot remove infowindow close button because element wasn't found in container:`, containerEl);
    return;
  }
  buttonEl.remove();
}
