import * as PushService from './PushService';
import * as Events from './Events';

/**
 * Broadcat SITE_CREATED
 * @param data
 * @param author
 */
export const broadcastSiteCreated = (data, author) => {
  PushService.send(Events.SITE_CREATED, author, data);
};

/**
 * Broadcat SITE_POSITION_UPDATED
 * @param data
 * @param author
 */
export const broadcastSitePositionUpdated = (data, author) => {
  PushService.send(Events.SITE_POSITION_UPDATED, author, data);
};

/**
 * Broadcat SITE_ETAT_UPDATED
 * @param data
 * @param author
 */
export const broadcastSiteEtatUpdated = (data, author) => {
  PushService.send(Events.SITE_ETAT_UPDATED, author, data);
};

/**
 * Broadcat REMARQUE_CREATED
 * @param data
 * @param author
 */
export const broadcastRemarqueCreated = (data, author) => {
  PushService.send(Events.REMARQUE_CREATED, author, data);
};

/**
 * Broadcat PHOTO_ADDED
 * @param data
 * @param author
 */
export const broadcastPhotoAdded = (data, author) => {
  PushService.send(Events.PHOTO_ADDED, author, data);
};

/**
 * Broadcat FICHE_OPENED
 * @param id
 * @param author
 */
export const broadcastFicheOpened = (id, author) => {
  PushService.send(Events.FICHE_OPENED, author, id);
};

/**
 * Broadcat FICHE_CLOSED
 * @param id
 * @param author
 */
export const broadcastFicheClosed = (id, author) => {
  PushService.send(Events.FICHE_CLOSED, author, id);
};

/**
 * Broadcat WHERE_R_U
 * @param author
 */
export const broadcastWhereAreYou = (author) => {
  PushService.send(Events.WHERE_R_U, author);
};
