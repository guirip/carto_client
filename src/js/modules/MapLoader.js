const apiKey = 'AIzaSyDAhxjaGnMf0Ld3ExgQYa20VvhSptRzTqM'

/**
 * mapLoader
 */
export const load = () => {
  const scriptEl = document.createElement('script');
  scriptEl.setAttribute(
    'src',
    `${location.protocol}//maps.googleapis.com/maps/api/js?key=${apiKey}&language=fr&callback=onGoogleMapsAPIInitialized`
  );
  document.head.appendChild(scriptEl);
  console.log('Google Maps API is loading...');
};
