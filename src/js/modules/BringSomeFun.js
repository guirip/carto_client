/**
 * BringSomeFun module
 */

const funnyNames = [
    'face de dindon',
    'p\'tit toy va',
    'imcilebé',
    'gros dégueulasse',
    'p\'tit con',
    'bourrin va',
  ],
  funnyNamesCount = funnyNames.length;

/**
 * Return a funny name
 * @return {string}
 */
export const gimmeAFunnyName = () => {
  return funnyNames[Math.round(Math.random() * (funnyNamesCount - 0.51))];
};