
import config from 'config';
import * as Utils from './Utils';
import * as Context from './Context';
import * as Messages from 'ui/Messages';
import { LINK_PARAM_NAME } from 'ui/SiteLink';

/**
 * ServiceHelper module
 */

/**
 * Display an error message
 * @param jqXhr
 * @param {string} context : (optional) provide additional contextual info
 */
export function displayRequestError(message, context) {
  if (message === 'Failed to fetch') {
    message = '';
  }
  Messages.error(`Erreur de la requête serveur. ${context || ''} ${message || ''}`);
}

/**
 * Redirects to login screen
 */
function sessionExpired() {
  const params = [];

  const linkedId = Utils.getQueryParameter(LINK_PARAM_NAME);
  if (linkedId) {
    params.push({ key: LINK_PARAM_NAME, value: linkedId })
  }

  Utils.goLogin(params);
}

/**
 * Add user as a request param
 */
const authorPath = () => `/author/${encodeURI(Context.getAuthor())}`

/**
 * Simple wrapper to handle errors in a generic way
 * @param  {string} url
 * @param  {object} opts
 * @param  {boolean} skip redirection on 401
 * @return {object} response
 */
const executeRequest = (url, opts, skip401Redirection) => (
  new Promise(function(resolve, reject) {

    let didTimeout = false;
    function handleTimeout() {
      didTimeout = true;
      reject(new Error('Délai dépassé'));
    }
    const fetchTimeoutId = window.setTimeout(handleTimeout, config.fetchTimeout || 10000);

    function clearFetchTimeout() {
      window.clearTimeout(fetchTimeoutId);
    }

    fetch(url, {
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
      credentials: 'include',
      ...opts
    })
    .then(async function(response) {
      clearFetchTimeout();

      switch (response.status) {
        case 400:
          throw new Error(await response.text());

        case 401:
          if (!skip401Redirection) {
            sessionExpired(); // redirect
            return;
          }
          break;

        default: // for linter
      }
      resolve(response);
    })
    .catch(function(e) {
      if (didTimeout) return;
      clearFetchTimeout();
      reject(e);
    })
  })
  .catch(function(e) {
    displayRequestError(e?.message);
  })
)

/**
 * Perform login
 * @param user
 * @param password
 */
export async function login(user, password) {
  const response = await executeRequest(`${config.backendPath}/login/${user}`, {
    method: 'POST',
    body: JSON.stringify({ secret: password }),
  }, true);

  return {
    status: response?.status,
    error: !response,
  };
}

/**
 * Check session login
 * @param user
 */
export async function checkSession() {
  const response = await executeRequest(`${config.backendPath}/login`, {
    method: 'GET',
  });

  return {
    data: await response?.json(),
    error: !response,
  };
}

/**
 * Perform logout
 */
export async function logout() {
  const response = await executeRequest(`${config.backendPath}/login`, {
    method: 'POST',
  });
  return {
    result: response?.status === 200,
    error: !response,
  };
}

/**
 * Get etats labels
 */
export async function getEtats() {
  const response = await executeRequest(`${config.backendPath}/etat`, {
    method: 'GET',
  });
  return {
    data: await response?.json(),
    error: !response,
  };
}

/**
 * Get sites
 * @param depts
 * @param etats
 */
export async function getSites(depts, etats) {
  const
    deptsString = depts.join(','),
    etatsString = etats.join(',');

 const response = await executeRequest(`${config.backendPath}/site/depts/${deptsString}/etats/${etatsString}`, {
    method: 'GET',
  });
  return {
    data: await response?.json(),
    error: !response,
  };
}

/**
 * Count sites
 * @param depts
 * @param etats
 */
export async function countSites(depts, etats) {
  const
    deptsString = depts.join(','),
    etatsString = etats.join(',');

 const response = await executeRequest(`${config.backendPath}/site/count/depts/${deptsString}/etats/${etatsString}`, {
    method: 'GET',
  });
  const { count } = await response?.json()
  return {
    count: parseInt(count, 10),
    error: !response,
  };
}

/**
 * Get sites around a position
 * @param  {float} longitude
 * @param  {float} latitude
 * @param  {int} distance
 * @param  {array} etats
 */
export async function getSitesAround(longitude, latitude, distance, etats) {
  const response = await executeRequest(
    `${config.backendPath}/site/near/lon/${longitude}/lat/${latitude}/dist/${distance}/etats/${etats.join(',')}`,
    {
      method: 'GET',
    }
  );
  return {
    data: await response?.json(),
    error: !response,
  };
}

/**
 * Get a site
 * @param id
 */
export async function getSite(id) {
  const response = await executeRequest(`${config.backendPath}/site/${id}`, {
    method: 'GET',
  });
  return {
    data: await response?.json(),
    error: !response,
  };
}

/**
 * Create a site
 * @param commune
 * @param dept
 * @param nom
 * @param adresse
 * @param longitude
 * @param latitude
 * @param etat
 * @param pays
 */
export async function createSite({
  commune,
  dept,
  nom,
  adresse,
  longitude,
  latitude,
  etat,
  pays
}) {
  const response = await executeRequest(`${config.backendPath}/site/create${authorPath()}`, {
    method: 'POST',
    body: JSON.stringify({
      COMMUNE: commune,
      DEPARTEMENT: dept,
      NOM_USUEL: nom,
      ADRESSE: adresse,
      ETAT: etat,
      PAYS: pays,
      longitude,
      latitude,
    }),
  });
  return {
    createdSite: await response?.json(),
    error: !response,
  };
}

/**
 * Update x,y position of a site
 * @param {string} id
 * @param {string} x
 * @param {string} y
 * @param {string} adresse
 * @param {string} commune
 * @param {string} departement
 * @param {string} pays
 */
export async function savePosition(id, x, y, adresse, commune, departement, pays) {
  const response = await executeRequest(`${config.backendPath}/site/${id}/position${authorPath()}`, {
    method: 'POST',
    body: JSON.stringify({ x, y, adresse, commune, departement, pays }),
  });
  return {
    data: await response?.json(),
    error: !response,
  };
}

/**
 * Update etat of a site
 * @param id
 * @param etat
 */
export async function saveEtat(id, etat) {
  const response = await executeRequest(`${config.backendPath}/site/${id}/etat${authorPath()}`, {
    method: 'POST',
    body: JSON.stringify({ etat, }),
  });
  return {
    data: await response?.json(),
    error: !response,
  };
}

/**
 * Get remarques about a site
 * @param {int} siteId
 */
export async function getRemarques(siteId) {
  const response = await executeRequest(`${config.backendPath}/remarque/site/${siteId}`, {
    method: 'GET',
  });
  return {
    data: await response?.json(),
    error: !response,
  };
}

/**
 * Save a remarque
 * @param {int} siteId
 * @param {string} remarque
 */
export async function createRemarque(siteId, remarque) {
  const response = await executeRequest(`${config.backendPath}/remarque/create/site/${siteId}${authorPath()}`, {
    method: 'POST',
    body: JSON.stringify({ remarque }),
  });
  return {
    data: await response?.json(),
    error: !response,
  };
}

/**
 * Get photos for a site
 * @param {int} siteId
 */
export async function getPhotos(siteId) {
  const response = await executeRequest(`${config.backendPath}/photo/site/${siteId}`, {
    method: 'GET',
  });
  return {
    data: await response?.json(),
    error: !response,
  };
}

/**
 * @param {int} page
 * @param {Object} opts
 */
export async function getDiaryEntries() {
  const response = await executeRequest(`${config.backendPath}/diary`, {
    method: 'GET',
  });
  return {
    data: await response?.json(),
    error: !response,
  };
}

/**
 */
export async function getDiag() {
  const response = await executeRequest(`${config.backendPath}/diag`, {
    method: 'GET',
  });
  return {
    data: await response?.json(),
    error: !response,
  };
}

/**
 * @param failureCallback
 */
export async function uploadPhoto(
  formData,
  siteId
) {
  // TODO - no JSON is sent

  const response = await executeRequest(`${config.backendPath}/photo/${siteId}${authorPath()}`, {
    method: 'POST',
    body: formData,
  });
  return {
    data: await response?.json(),
    error: !response,
  };
}

export async function executeGraphQl(query) {
  const response = await executeRequest(`${config.backendPath}/graphql`, {
    method: 'POST',
    body: JSON.stringify({ query })
  });
  return {
    data: await response?.json(),
    error: !response,
  };
}
global.executeGraphQl = executeGraphQl;

/**
 * Send an email
 * @param {String} title
 * @param {String} message
 */
export async function sendEmail(title, message) {
  const response = await executeRequest(`${config.backendPath}/email`, {
    method: 'POST',
  });
  return {
    data: await response?.json(),
    error: !response,
  };
}

/**
 * Retrieve push events since a given date/time (useful for reconnection process)
 * @param  {number}   dateTimeInMs
 * @param  {Function} next
 */
export async function recoverEventsSince(dateTimeInMs) {
  const response = await executeRequest(`${config.wsUrl}/since/${dateTimeInMs}`, {
    method: 'GET',
  });
  return {
    data: await response?.json(),
    error: !response,
  };
}
