import * as Utils from './Utils';

// import Modernizr from 'lib/modernizr-custom';
const Modernizr = require('../lib/modernizr-custom');

const isMsPointerSupported =
  typeof window.navigator.msPointerEnabled !== 'undefined';

let interaction;

if (Modernizr.pointerevents === true) {
  interaction = {
    start: 'pointerdown',
    move: 'pointermove',
    end: 'pointerup',
  };
} else if (isMsPointerSupported === true) {
  interaction = {
    start: 'MSPointerDown',
    move: 'MSPointerMove',
    end: 'MSPointerUp',
  };
} else {
  interaction = {
    start: 'touchstart',
    move: 'touchmove',
    end: 'touchend',
  };
}

interaction.click =
  Modernizr.pointerevents === true
    ? 'pointerdown'
    : Modernizr.touchevents
    ? 'touchstart'
    : 'click';

/**
 * Return the distance between 2 touches
 * @param  {array} events
 * @return {number}
 */
function eventsDist(events) {
  if (events.length < 2) {
    return 0;
  }
  let prefix = 'client';
  if (typeof events[0].clientX !== 'number') {
    prefix = 'page';
  }
  let coord1 = {
      x: events[0][prefix + 'X'],
      y: events[0][prefix + 'Y'],
    },
    coord2 = {
      x: events[1][prefix + 'X'],
      y: events[1][prefix + 'Y'],
    };

  return mgUtils.dist(coord1, coord2);
}

/**
 * Calculate distance
 * @param  {object} coord1 as: { x:number, y:number }
 * @param  {object} coord2 as: { x:number, y:number }
 * @return {number}
 */
function getDistance(coord1, coord2) {
  // Check coord1 argument
  if (coord1 === null || typeof coord1 !== 'object') {
    Utils.logError(
      "Cannot calculate distance, 'coord1' is not an object",
      coord1
    );
  }
  if (typeof coord1.x !== 'number') {
    Utils.logError(
      "Cannot calculate distance, 'coord1.x' is not a number",
      coord1
    );
  }
  if (typeof coord1.y !== 'number') {
    Utils.logError(
      "Cannot calculate distance, 'coord1.y' is not a number",
      coord1
    );
  }

  // Check coord2 argument
  if (coord2 === null || typeof coord2 !== 'object') {
    Utils.logError(
      "Cannot calculate distance, 'coord2' is not an object",
      coord1
    );
  }
  if (typeof coord2.x !== 'number') {
    Utils.logError(
      "Cannot calculate distance, 'coord2.x' is not a number",
      coord2
    );
  }
  if (typeof coord2.y !== 'number') {
    Utils.logError(
      "Cannot calculate distance, 'coord2.y' is not a number",
      coord2
    );
  }

  return Math.sqrt(
    Math.pow(coord1.x - coord2.x, 2) + Math.pow(coord1.y - coord2.y, 2)
  );
}

export function addTapEvent(el, cb) {
  if (!el) {
    Utils.logError('Missing DOM element');
    return;
  }
  if (typeof cb !== 'function') {
    Utils.logError('Missing callback');
    return;
  }

  if (interaction.click === 'click') {
    // Easy: simple addEventListener
    el.addEventListener(interaction.click, cb);
  } else {
    // Touch
    // Tap event is valid if the following sequence is met:
    //  * start
    //  * no move
    //  * end
    const MOVE_THRESHOLD = 10;
    let
      eventStart = interaction.start,
      eventMove = interaction.move,
      eventEnd = interaction.end,
      lastEventReceived,
      startEvent;

    // Start
    el.addEventListener(eventStart, function (e) {
      lastEventReceived = eventStart;
      startEvent = e;
    });

    // Move
    el.addEventListener(eventMove, function (e) {
      if (!startEvent) {
        // If the event sequence has not started on `el`, skip
        return;
      }
      // When a certain move distance is reached, then we cancel the tap
      if (getDistance([startEvent, e]) > MOVE_THRESHOLD) {
        lastEventReceived = eventMove;
      }
    });

    // End
    el.addEventListener(eventEnd, function (e) {
      // Skip if a move event has taken place in the sequence
      if (lastEventReceived === eventStart) {
        lastEventReceived = eventEnd;
        cb(e);
      }
      startEvent = null;
    });
  }
}
