/**
 * Context module
 */

export const getAuthor = () => localStorage.author;

/**
 * @param value
 */
export const setAuthor = (value) => {
  localStorage.author = value;
};

export const getCurrentFicheId = () => localStorage.currentFicheId;

/**
 * @param value
 */
export const setCurrentFicheId = (value) => {
  localStorage.currentFicheId = value;
};

let currentDragId;

export const getCurrentDragId = () => currentDragId;

/**
 * @param value
 */
export const setCurrentDragId = (value) => {
  currentDragId = value;
};
