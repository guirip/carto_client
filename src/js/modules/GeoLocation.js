import PubSub from 'pubsub-js';

import * as Events from 'modules/Events';

const REFRESH_INTERVAL = 3500;

let watchID;

export function start() {
  if (!navigator.geolocation) {
    PubSub.publish(Events.USER_LOCATION_ERROR);
    return;
  }

  watchID = navigator.geolocation.watchPosition(watchPositionSuccess, watchPositionError);
}

function watchPositionSuccess(position) {
  const pos = {
    lat: position.coords.latitude,
    lng: position.coords.longitude,
    heading: position.coords.heading,
  };
  PubSub.publish(Events.USER_LOCATION, pos);
}

function watchPositionError(error) {
  PubSub.publish(Events.USER_LOCATION_ERROR, error);
}

export function stop() {
  if (watchID) {
    navigator.geolocation.clearWatch(watchID);
    watchID = null;
  }
}
