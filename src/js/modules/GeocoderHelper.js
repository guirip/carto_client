import PubSub from 'pubsub-js';

import { POSTAL_CODE_FOR_FOREIGN_COUNTRIES } from './Departements';
import * as Events from './Events';
import * as Messages from 'ui/Messages';

/**
 * GeocoderHelper module
 */

let geocoder;

const constants = {
  rue_numero: 'street_number',
  rue: 'route',
  commune: 'locality',
  departement: 'administrative_area_level_2',
  region: 'administrative_area_level_1',
  pays: 'country',
  code_postal: 'postal_code',
};

/**
 * Perform a geocode request
 * @param data
 */
export async function performGeocode(opts) {
  if (!geocoder) {
    throw new Error('geocoder has not been initialized yet.');
  }
  try {
    const {results} = await geocoder.geocode(opts);

    if (!results || results.length === 0) {
      Messages.warn('Aucun résultat');
      return;
    }
    return results[0];

  } catch (e) {
    console.error(`Geocoding failed`, e);

    switch (e.code) {
      case 'ZERO_RESULTS':
        Messages.warn('Aucun résultat');
        break;
      case 'OVER_QUERY_LIMIT':
        Messages.warn('Limite de service google<br>Réessaie dans 2-3 secondes 🕑');
        break;
      default:
        Messages.error(`Erreur lors de la recherche d'adresse auprès de google : ${e.message}`);
    }
    return null
  }
}

const PLUS_CODE_REGEXP = /[A-Z0-9]{4}\+[A-Z0-9]{2}/;

/**
 * Parse geocode result to extract needed data
 *
 * @param  {object} geocodeResult
 * @return  {object} such as { adresse, commune, dept, }
 */
export function getAddressObject(geocodeResult) {
  let
    compo,
    adresse = geocodeResult.formatted_address ?? '',
    commune,
    dept,
    pays;

  if (/unnamed road/i.test(adresse)) {
    adresse = '';
  } else {
    // Clean up useless Google maps plus_code (https://maps.google.com/pluscodes/)
    adresse = adresse.replace(PLUS_CODE_REGEXP, '').trim();
  }

  // Pays
  compo = getAddressField(constants.pays, geocodeResult.address_components);
  if (compo) {
    pays = compo.short_name;

    if (adresse && compo.short_name === 'FR') {
      adresse = adresse.replace(', France', '');
    }
  }
  // Special case for foreign countries
  if (!compo || compo.short_name !== 'FR') {
    dept = POSTAL_CODE_FOR_FOREIGN_COUNTRIES;
  }
  else {
    // Departement
    dept = getDepartementFromResultComponents(geocodeResult.address_components);
  }

  // Commune
  compo = getAddressField(constants.commune, geocodeResult.address_components);
  if (compo) {
    commune = compo.long_name;
  }

  return {
    adresse,
    commune,
    dept,
    pays,
  };
}

/**
 * Common function to retrieve departement from geocode results
 * @param  {object} components
 * @return {string}
 */
function getDepartementFromResultComponents(components) {
  const compo = getAddressField(constants.code_postal, components);
  if (compo) {
    return compo.short_name.substr(0, compo.short_name.length === 4 ? 1 : 2);
  }
}

const getAddressField = (field, compos) => compos.find(c => c.types.includes(field))

/**
 * Initialize geocoder
 */
const initGeocoder = function () {
  geocoder = new google.maps.Geocoder();
};
PubSub.subscribe(Events.MAP_READY, initGeocoder);
