/**
 * Events module
 */

export const AUTH_SUCCESS = 'AUTH_SUCCESS';

// Site events
export const SITE_FOCUS = 'SITE_FOCUS'; // OPEN FICHE + ZOOM
export const SITE_ZOOM_CURRENT = 'SITE_ZOOM_CURRENT'; // ZOOM ON CURRENT SITE
export const SITE_ZOOM = 'SITE_ZOOM'; // ZOOM ON A GIVEN SITE
export const GET_SITES = 'GET_SITES';
export const GET_SITES_AROUND = 'GET_SITES_AROUND';
export const REFRESH_SITES = 'REFRESH_SITES';

// Site creation
export const SITE_CREATION_SHOW = 'SITE_CREATION_SHOW';

// Map events
export const MAP_PAN_TO = 'MAP_PAN_TO';
export const MAP_ZOOM_TO = 'MAP_ZOOM_TO';
export const MAP_READY = 'MAP_READY';
export const MARKER_DRAGGABLE = 'MARKER_DRAGGABLE';
export const MARKER_MOVE = 'MARKER_MOVE';
export const MAP_ADD_SITE_TOOLTIP = 'MAP_ADD_SITE_TOOLTIP';
export const MAP_SHOW_TOOLTIP = 'MAP_SHOW_TOOLTIP';
export const MAP_STREETV_OPEN = 'MAP_STREETV_OPEN';
export const MAP_STREETV_CLOSE = 'MAP_STREETV_CLOSE';
export const MAP_LONG_PRESS = 'MAP_LONG_PRESS';
export const MARKER_GO_TO_PREVIOUS = 'MARKER_GO_TO_PREVIOUS';
export const MARKER_GO_TO_NEXT = 'MARKER_GO_TO_NEXT';

// Location events
export const USER_LOCATION = 'USER_LOCATION';
export const USER_LOCATION_ERROR = 'USER_LOCATION_ERROR';

// Window events
export const BODY_CLICKED = 'BODY_CLICKED';
export const WINDOW_RESIZE = 'WINDOW_RESIZE';

// WS events
export const PUSH_EVENT = 'PUSH_EVENT';
export const SITE_CREATED = 'S_CR';
export const SITE_POSITION_UPDATED = 'S_PU';
export const SITE_ETAT_UPDATED = 'S_EU';
export const REMARQUE_CREATED = 'R_CR';
export const PHOTO_ADDED = 'P_AD';
export const FICHE_OPENED = 'F_OP';
export const FICHE_CLOSED = 'F_CL';
export const WHERE_R_U = 'WRU';
