import PubSub from 'pubsub-js';
import io from 'socket.io-client';

import config from 'config';
import * as Utils from './Utils';
import * as Events from './Events';
// import * as Messages from 'ui/Messages';
// import { recoverEventsSince } from 'modules/ServiceHelper';

/**
 * PushService module
 */

const opts = {
  reconnection: true,
};
const waitingLine = [];
const timestamps = {};

let socket;

/**
 * Open connection and initialize handlers
 */
export const init = function() {
  performInitialization();
};

/**
 * Initialize WebSocket object
 */
const performInitialization = function() {
  socket = io(config.wsUrl, opts);

  // Connection opened
  socket.on('connect', function() {
    console.log('[WS] Welcome - status ' + socket.io.readyState);

    if (timestamps.disconnected) {
      // Messages.info('Connexion au serveur récupérée');
      recoverEvents(timestamps.disconnected);
    }

    timestamps.connected = new Date().getTime();
    timestamps.disconnected = null;

    while (waitingLine.length) {
      _send(waitingLine.shift());
    }
  });

  // Message received
  socket.on('event', function(event) {
    // Parse JSON
    const json = JSON.parse(event);
    if (
      json.data &&
      typeof json.data === 'string' &&
      json.data.indexOf(':') != -1
    ) {
      json.data = JSON.parse(json.data);
    }

    // Propagate
    PubSub.publish(json.type, json);

    // For diary
    PubSub.publish(Events.PUSH_EVENT, json);

    timestamps.lastEventReceived = new Date().getTime();
  });

  // Error handler
  socket.on('error', logError);
  socket.on('connect_error', logError);
  socket.on('reconnect_error', logError);
  socket.on('reconnect_failed', logError);
  socket.on('disconnect', handleConnectionError);
};

function logError(error) {
  Utils.log(`[WS error] ${error}`);
}

/**
 * @param  error
 */
const handleConnectionError = function(error) {

  if (!timestamps.disconnected) {
    timestamps.disconnected = new Date().getTime();
  }

  // TODO
  // Set diag WS status KO
  /*if (!timestamps.disconnected) {
    timestamps.disconnected = new Date().getTime();

    Messages.warn('Connexion au serveur perdue <small>('+socket.io.readyState+')<small>', { delay: 0 });
    console.error(error);
  }*/
};

/**
 * Send a message to broadcast
 *
 * @param type
 * @param author
 * @param data
 */
export const send = function(type, author, data) {
  if (!socket) {
    waitingLine.push(getString(type, author, data));
    init();
  } else {
    _send(getString(type, author, data));
  }
};

/**
 * [_send description]
 * @param  message
 */
const _send = function(message) {
  socket.emit('event', message);
};

/**
 * @param type
 * @param author
 * @param data
 */
const getString = function(type, author, data) {
  return JSON.stringify({
    type: type,
    author: author,
    data: typeof data == 'string' ? data : JSON.stringify(data),
  });
};

/**
 * Close web socket connection
 *
 */
export const quit = function() {
  if (socket) {
    socket.disconnect();
    console.log('[WS] Quit - status ' + socket.io.readyState);
    socket = null;
  }
};

/**
 * @param bool : value to set for autoreconnect
 */
export const setAutoReconnect = function(bool) {
  socket.reconnection(bool);
};

/**
 * @param  {number} since
 */
async function recoverEvents(since) {

  // Chrome forces a page refresh
  return;

  let { events, error } = await ServiceHelper.recoverEventsSince(since);
  if (error) return;

  console.log('recovered events: ', events);
}
