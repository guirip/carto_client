'use strict';

const prodConfig = {
  backendPath: `/backend`, // apache proxy config
  wsUrl: `https://carto-integration.heytonton.fr:3004`,
};

module.exports = require('deepmerge')(
  require('./config.common'),
  prodConfig
);
