'use strict';

const devConfig = {
  backendPath: `http://${location.hostname}:3010`,
  wsUrl: `http://${location.hostname}:3001`,
};

module.exports = require('deepmerge')(
  require('./config.common'),
  devConfig
);
