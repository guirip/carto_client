
// FIXME MODULE NON MAINTENU

/*
import PubSub from 'pubsub-js';
let juicebox = require('../lib/juicebox.js');

import config from 'config';

import * as Utils from 'modules/Utils';
import * as Context from 'modules/Context';
// import * as ServiceHelper from 'modules/ServiceHelper';

import diapoTpl from 'templates/diapo.html';

import 'style/diapo.scss';

/**
 * Diapo module
 *

let $container,
  $jbContainer,
  jbContainerId = 'juicebox-container',
  // @see http://www.juicebox.net/support/config_options/
  jbOpts = {
    containerId: jbContainerId,
    baseUrl: config.staticPath + config.photoDir,
    themeUrl: 'css/lib/juicebox/juicebox-theme.css',
  },
  templateData = {
    closeLabel: 'Quitter le diaporama',
  };

export const events = {
  SHOW: 'DIAPO_SHOW',
};

/**
 * Show diapo
 * @param  {string} event
 * @param  {object} data as { ficheId: ..., photoId: ...}
 *
const show = (event, data) => {
  if (!$container) {
    $container = $(diapoTpl(templateData));
    $container.addEventListener('click', '#diapo-close', hide);

    $jbContainer = $container.find('#' + jbContainerId);
  }

  jbOpts.galleryTitle = data.title;
  jbOpts.configUrl =
    config.staticPath + config.photoDir + '/' + data.siteId + '.xml';
  // jbOpts.firstImageIndex = TODO

  new juicebox(jbOpts);

  $container[0].style.display = 'block';
  Utils.appendAndShowModule($container);
  enableKeydownListener();

  // FIXME: focus to allow keyboard navigation
};
PubSub.subscribe(events.SHOW, show);

/**
 * Hide diapo
 *
const hide = () => {
  $container[0].style.display = 'none';
  disableKeydownListener();
};

const enableKeydownListener = () => {
  $(document).on('keydown', keydownEventHandler);
};

const disableKeydownListener = () => {
  $(document).off('keydown', keydownEventHandler);
};

/**
 * Handle keydown events, if ESCAPE key is pressed then close diapo
 * @param {event} e
 *
const keydownEventHandler = (e) => {
  if (e.keyCode == 27) {
    // ESCAPE keyboard key
    hide();
  }
};
*/