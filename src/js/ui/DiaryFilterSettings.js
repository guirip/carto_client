/**
 * This module wraps and persists diary filtered event types
 */

import * as Events from '../modules/Events';


const DEFAULT_CONF = {
  [Events.SITE_CREATED]: 1,
  [Events.SITE_POSITION_UPDATED]: 1,
  [Events.SITE_ETAT_UPDATED]: 1,
  [Events.REMARQUE_CREATED]: 1,
  [Events.PHOTO_ADDED]: 1,
};

const LS_KEY = 'diary-setting';

let actual;

/**
 * Return current settings
 * @return {object}
 */
export const get = () => {
  if (!actual) {
    // Read from localStorage
    const valueFromLS = localStorage.getItem(LS_KEY);
    if (valueFromLS) {
      actual = JSON.parse(valueFromLS);
    } else {
      // Init localStorage value
      updateLS(DEFAULT_CONF);
      actual = DEFAULT_CONF;
    }
  }
  return actual;
};

/**
 * Indicate if this type is displayed
 * @param  {[type]} type
 * @return {boolean}
 */
export const isActive = (type) => {
  return get()[type] === 1; // calling get() allows to init if needed
};

/**
 * Update setting for a type
 * @param  {[type]} type
 * @param  {number} value
 */
export const update = (type, value) => {
  if (get()[type] !== value) {
    // calling get() allows to init if needed
    actual[type] = value;
    updateLS(actual);
  }
};

/**
 * Update value in localStorage
 * @param  {object} value
 */
const updateLS = (value) => {
  localStorage.setItem(LS_KEY, JSON.stringify(value));
};
