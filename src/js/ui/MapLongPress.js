import PubSub from 'pubsub-js';

import * as Events from 'modules/Events';

const
  DELAY = 1000,
  DRAG_CANCEL_THRESHOLD = 30;

let timeout, startPixels, startLatLng;

export function start(e) {
  if (timeout) {
    // Means several fingers on the screen
    cancel();
    return;
  }

  startPixels = e.pixel;
  startLatLng = e.latLng;
  timeout = window.setTimeout(timerEnd, DELAY);
}

// RAF ?
/*export function mapDragged(e) {
  const
    deltaX = Math.abs(e.pixel.x - startPixels.x),
    deltaY = Math.abs(e.pixel.y - startPixels.y);

  console.log('map dragged '+deltaX+' x pixel and '+deltaY+' y pixels');

  if (deltaX > DRAG_CANCEL_THRESHOLD || deltaY > DRAG_CANCEL_THRESHOLD) {
    cancel();
    console.log('long press cancel');
  }
}*/

export function cancel() {
  if (timeout) {
    window.clearTimeout(timeout);
    timeout = null;
  }
}

function timerEnd() {
  PubSub.publish(Events.MAP_LONG_PRESS, startLatLng);
}

/*
OLD CODE WHICH REACTS WHEN THE FINGER IS RELEASED

let mouseDownTime,
    mouseDownPixel;

const mapMouseDownHandler = function(e) {
    mouseDownTime = new Date().getTime();
    mouseDownPixel = e.pixel;
};

const mapMouseUpHandler = function(e) {
    if (mouseDownTime) {
        if (new Date().getTime() - mouseDownTime > 1500) {
            if ((Math.abs(mouseDownPixel.x - e.pixel.x) < 20)
                    && (Math.abs(mouseDownPixel.y - e.pixel.y) < 20)) {

                // long press detected
                mouseDownTime = null;
                mouseDownPixel = null;

                window.setTimeout(mapRightClickHandler, 50, e); // timer needed because of Utils.clickOutHandler
            }
        }
    }
};
*/
