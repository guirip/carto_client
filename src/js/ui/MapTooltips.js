import PubSub from 'pubsub-js';

import * as Events from 'modules/Events';
import * as Utils from 'modules/Utils';

import 'style/tooltips.scss';

/**
 * MapsTooltips module
 */

const ttps = {},
  selectedZIndex = 2;

export const events = {
  ADD: 'TTP_ADD',
  REMOVE: 'TTP_REMOVE',
};

const getTooltipId = (siteId) => `site-tooltip-${siteId}`

const getContent = (siteId, users) => (
  `<div id="${getTooltipId(siteId)}" class="site-tooltip">${users.join(', ')}</div>`
)

/**
 * @param {string} siteId
 * @param {string} username
 */
const addToolTip = (siteId, username) => {
  if (!ttps[siteId]) {
    // Create tooltip
    ttps[siteId] = new google.maps.InfoWindow({
      zIndex: selectedZIndex,
      content: getContent(siteId, [username]),
    });
    ttps[siteId].id = siteId;
    ttps[siteId].users = [ username ];

    ttps[siteId].addListener('domready', function () {
      const el = document.getElementById(getTooltipId(siteId));
      Utils.removeInfowindowCloseIcon(el);
    });

  } else {
    // Update tooltip by only adding this username
    addUser(siteId, username);
  }
  PubSub.publish(Events.MAP_ADD_SITE_TOOLTIP, ttps[siteId]);
};

/**
 * Handle ADD event
 * @param event
 * @param data
 */
const addTooltipEventHandler = (event, data) => {
  addToolTip(data.data, data.author);
};
PubSub.subscribe(events.ADD, addTooltipEventHandler);


/**
 * @param  {string} siteId
 * @param  {string} username
 * @return {boolean}
 */
function tooltipHasUsername(siteId, username) {
  return ttps[siteId].users.indexOf(username) !== -1;
}

/**
 * Apply to template to display up-to-date users list
 * @param  {string} siteId
 */
function refreshTooltip(siteId) {
  ttps[siteId].setContent(getContent(ttps[siteId].id, ttps[siteId].users))
}

/**
 * Add a user to an existing tooltip
 * @param {string} siteId
 * @param {string} username
 */
const addUser = (siteId, username) => {
  if (tooltipHasUsername(siteId, username) !== true) {
    ttps[siteId].users.push(username);
    refreshTooltip(siteId);
  }
};

/**
 * @param {string} siteId
 * @param {string} username
 */
const removeUserFromToolTip = (siteId, username) => {
  if (ttps[siteId]) {
    if (ttps[siteId].users.length > 1) {
      // Update tooltip by removing only this user name
      removeUser(siteId, username);
    } else {
      removeTooltip(siteId);
    }
  }
};

/**
 * Handle FICHE_CLOSED event
 * @param event
 * @param data
 */
const ficheClosedHandler = (event, data) => {
  removeUserFromToolTip(data.data, data.author);
};
PubSub.subscribe(Events.FICHE_CLOSED, ficheClosedHandler);

/**
 * Remove a user from an existing tooltip
 * @param {string} siteId
 * @param {string} username
 */
const removeUser = (siteId, username) => {
  const index = ttps[siteId].users.findIndex(iUsername => iUsername === username);
  if (typeof index === 'number' && index !== -1) {
    ttps[siteId].users.splice(index, 1);
    refreshTooltip(siteId);
  }
};

/**
 * Force tooltip delete for a given id
 */
function removeTooltip(siteId) {
  if (ttps[siteId]) {
    ttps[siteId].close();
    delete ttps[siteId];
  }
};
PubSub.subscribe(events.REMOVE, function(_eventName, siteId) {
  removeTooltip(siteId);
});
