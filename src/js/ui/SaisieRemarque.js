import PubSub from 'pubsub-js';

import {
	ClassicEditor,
	Autoformat,
	Bold,
	Italic,
	Underline,
	Essentials,
	Heading,
	Link,
	Mention,
	Paragraph,
	PasteFromOffice,
	TextTransformation,
} from 'ckeditor5';
import coreTranslations from 'ckeditor5/translations/fr.js';
import 'ckeditor5/ckeditor5.css';

import {gimmeAFunnyName} from 'modules/BringSomeFun';
import * as BroadcastHelper from 'modules/BroadcastHelper';
import * as Context from 'modules/Context';
import * as ServiceHelper from 'modules/ServiceHelper';
import * as Utils from 'modules/Utils';

import * as Messages from './Messages';

import saisieRemarqueTpl from 'templates/saisieRemarque.html';

/**
 * SaisieRemarque module
 */

// Local variables
let saisieRemarque, editor;

const tplSaisieData = {
  closeLabel: 'Annuler',
  remarqueLabel: 'Commentaire',
  validerLabel: 'Valider',
};

export const events = {
  SHOW: 'SAISIE_REM_SHOW',
};

/**
 * Initialize module
 */
async function init() {
  await import('style/saisie-remarque.scss');

  saisieRemarque = Utils.tplToDom(saisieRemarqueTpl, tplSaisieData);
  saisieRemarque.querySelector('.popup-close').addEventListener('click', hide);
  saisieRemarque.querySelector('.btn-valider').addEventListener('click', saveRemarque);
  document.body.appendChild(saisieRemarque);

  editor = await ClassicEditor
    .create(saisieRemarque.querySelector('input'), {
        plugins: [
          Autoformat,
          Bold,
          Essentials,
          Heading,
          Italic,
          Link,
          Mention,
          Paragraph,
          PasteFromOffice,
          TextTransformation,
          Underline,
        ],
        toolbar: [
          'undo',
          'redo',
          '|',
          'bold',
          'italic',
          'underline',
          '|',
          'link',
          '|',
          'heading',
        ],

        // This value must be kept in sync with the language defined in webpack.config.js.
        language: 'fr',
        translations: [
          coreTranslations,
      ]
    } );
}

const show = () => {
  editor.setData('');
  Utils.showPopup(saisieRemarque);
};
PubSub.subscribe(events.SHOW, show);

const hide = () => {
  Utils.hidePopup(saisieRemarque);
};

async function saveRemarque({target}) {
  const text = editor.getData();
  if (!text) {
    Messages.warn(`Saisis d'abord quelque-chose, ${gimmeAFunnyName()} !`);
    return;
  }

  target.disabled = true;
  const { data:createdRemarque, error } = await ServiceHelper.createRemarque(
    Context.getCurrentFicheId(),
    text
  );
  window.setTimeout(() => {
    // re-enable the button after a delay (else it can be clicked again when the popup is hiding)
    target.disabled = false;
  }, 200);

  if (error) return;

  hide();
  BroadcastHelper.broadcastRemarqueCreated(createdRemarque, Context.getAuthor());
}

Utils.executeOnDomReady(init);
