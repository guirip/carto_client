import PubSub from 'pubsub-js';

import * as Events from 'modules/Events';
import * as Utils from 'modules/Utils';

import { events as aroundEvents } from './Around';
import * as EtatsList from './EtatsList';
import * as Filter from './Filter';

import menuTpl from 'templates/menu.html';

import 'style/menu.scss';
import 'style/infowindow.override.scss';

/**
 * Menu module
 */

// Local variables
let
  menuIB,
  showTimestamp;

export const events = {
  SHOW: 'MENU_SHOW',
};


/**
 * Initialize the contextual menu appearing on right click or long press on map
 */
const initMenu = function () {
  if (!EtatsList.isReady()) {
    EtatsList.doWhenReady(initMenu);
  } else {
    menuIB = new google.maps.InfoWindow({
      zIndex: 2,
      content: menuTpl(null, true),
    });
    menuIB.addListener('domready', function () {
      const menuEl = document.getElementById('menu');
      Utils.removeInfowindowCloseIcon(menuEl);
      menuEl.querySelector('#menu-add').addEventListener('click', addBtnClicked);
      menuEl.querySelector('#menu-filter').addEventListener('click', filterBtnClicked);
      menuEl.querySelector('#menu-around').addEventListener('click', aroundBtnClicked);
    });
  }
};

/**
 * @param _event
 * @param data
 */
const showMenu = (_event, data) => {
  import('ui/SiteCreation')

  PubSub.publish(Events.MAP_SHOW_TOOLTIP, { ttp: menuIB, position: data.latLng });
  showTimestamp = new Date().getTime();
};
PubSub.subscribe(events.SHOW, showMenu);

/**
 * Close/hide the menu
 */
function hideMenu() {
  menuIB.close();
  showTimestamp = null;
}

/**
 * @param e
 */
const addBtnClicked = (e) => {
  e.stopPropagation();
  const position = menuIB.getPosition();
  hideMenu();
  PubSub.publish(Events.SITE_CREATION_SHOW, position);
};

/**
 * Send event to open filter
 */
const filterBtnClicked = () => {
  PubSub.publish(Filter.events.SHOW);
  hideMenu();
};

/**
 * Around button click handler
 */
function aroundBtnClicked(e) {
  e.stopPropagation();
  const position = menuIB.getPosition();
  const lat = position.lat();
  const lng = position.lng();

  hideMenu();
  PubSub.publish(aroundEvents.SHOW, { lat, lng });
};


/**
 * @param _event
 * @param target
 */
const bodyClicked = (_event, target) => {
  if (showTimestamp && new Date().getTime() - showTimestamp < 200) {
    // ignore the right click event
    return;
  }

  // Close menu
  if (menuIB
      && Utils.findParentNode(target, el => el.id === 'menu') === null) {
    hideMenu();
  }
};

PubSub.subscribe(Events.BODY_CLICKED, bodyClicked);

initMenu();
