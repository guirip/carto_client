import PubSub from 'pubsub-js';
import debounce from 'just-debounce';

import Departements, {POSTAL_CODE_FOR_FOREIGN_COUNTRIES} from 'modules/Departements';
import * as Events from 'modules/Events';
import * as GeocoderHelper from 'modules/GeocoderHelper';
import * as ServiceHelper from 'modules/ServiceHelper';
import * as Utils from 'modules/Utils';

import * as EtatsList from './EtatsList';
import * as ListHelper from './ListHelper';
import { events as DiaryEvents } from './Diary';

import etatsListTpl from 'templates/etatsList.html';
import filterTpl from 'templates/filter.html';

import { addTapEvent } from 'modules/Interaction';

/**
 * Filter module
 */

// Local variables
const
  STORAGE_DEPTS_KEY = 'filter-depts',
  STORAGE_ETATS_KEY = 'filter-etats',
  DEFAULT_DEPTS = [77, 78, 91, 92, 93, 94, 95],
  COUNTER_DELAY = 1200;

let container,
  tabDeptsEl,
  tabEtatsEl,
  deptsContainer,
  deptsSelectedContainer,
  deptsSelectedList,
  deptsAvailableList,
  selectedDepts = [],
  etatsContainer,
  etatsList,
  selectedEtats = [],
  btnContainer,
  btnValider,
  counterSpinner,
  counterValue;

const tplData = {
  depts: Departements,
  closeLabel: 'Fermer',
  departementsLabel: 'Départements',
  clearTitle: 'Vider les départements sélectionnés',
  clearLabel: 'vider',
  selectedLabel: 'Sélectionnés',
  etatsLabel: 'Etats',
  validerLabel: 'Afficher',
  validerTitle: 'Afficher les sites correspondants',
};

export const events = {
  SHOW: 'FILTER_SHOW',
  HIDE: 'FILTER_HIDE',
};

async function show() {

  // Check that EtatsList module is ready
  if (!EtatsList.isReady()) {
    EtatsList.doWhenReady(show);
  } else {
    if (!container) {
      await import('style/filter.scss');

      tplData.htmlEtatsList = etatsListTpl({
        etats: EtatsList.getEtats(),
      });

      tplData.etatsVisible = !Utils.isMobileDevice();

      // Apply template
      container = Utils.tplToDom(filterTpl, tplData);
      container.querySelector('.popup-close').addEventListener('click', hide);

      // DEPARTEMENTS

      deptsContainer = container.querySelector('#f-depts-container');

      deptsContainer.querySelector('#f-depts-clear-btn').addEventListener('click', emptySelectedDept);

      // Selected list
      deptsSelectedContainer = deptsContainer.querySelector('#f-depts-selected');
      deptsSelectedList = Utils.findChildren(
        deptsSelectedContainer,
        el => el.nodeName.toLowerCase() === 'ul'
      )[0];
      let lis = deptsSelectedList.querySelectorAll('li');
      for (const li of lis) {
        li.addEventListener('click', unselectDept);
      }

      // Available departements list
      deptsAvailableList = deptsContainer.querySelector('#f-depts-available');
      lis = deptsAvailableList.querySelectorAll('li');
      for (const li of lis) {
        li.addEventListener('click', selectDept);
      }

      // Set selected departements
      const deptsFromStorage = localStorage.getItem(STORAGE_DEPTS_KEY),
        deptsToSelect = deptsFromStorage
          ? JSON.parse(deptsFromStorage)
          : DEFAULT_DEPTS;
      deptsToSelect.forEach((dept) => {
        selectDeptByNumber(dept);
      });

      // Etats list
      etatsContainer = container.querySelector('#f-etats-container');
      etatsList = container.querySelector('.etats-list');
      lis = etatsList.querySelectorAll('li');
      for (const li of lis) {
        li.addEventListener('click', toggleAvailable);
      }

      // init selected etats
      const etatsToSelect = getSelectedEtats();

      etatsToSelect.forEach((etatId) => {
        const li = etatsList.querySelector('li[data-id="' + etatId + '"]');
        toggleAvailable({ target: li });
      });

      // Counter
      counterSpinner = container.querySelector('.f-counter-spinner');
      counterValue = container.querySelector('.f-counter-value');

      // Validate
      btnContainer = container.querySelector('#f-bottom-bar');
      btnValider = btnContainer.querySelector('.btn-valider');
      btnValider.addEventListener('click', btnValiderClick);

      // Add container to DOM
      document.body.appendChild(container);

      // Tabs
      tabDeptsEl = container.querySelector('#f-tab-depts');
      tabEtatsEl = container.querySelector('#f-tab-etats');

      if (tabDeptsEl) {
        addTapEvent(tabDeptsEl, activateDeptsTab);
      }
      if (tabEtatsEl) {
        addTapEvent(tabEtatsEl, activateEtatsTab);
      }
    }

    container.style.display = 'flex';
    container.style.opacity = 1;

    counterValue.style.display = 'none';
    performCount();
  }
}
PubSub.subscribe(events.SHOW, show);

const hide = () => {
  if (container) {
    Utils.closeModule(container);
    btnValider.disabled = false;
    // container.classList.remove('display-flex');
  }
};
PubSub.subscribe(events.HIDE, hide);
PubSub.subscribe(DiaryEvents.SHOW, hide);
// Hide module when map going in street view mode
PubSub.subscribe(Events.MAP_STREETV_OPEN, hide);


const selectedTabClass = 'f-tab-selected';
function activateDeptsTab() {
  toggleTab(tabDeptsEl, tabEtatsEl, deptsContainer, etatsContainer);
}
function activateEtatsTab() {
  toggleTab(tabEtatsEl, tabDeptsEl, etatsContainer, deptsContainer);
}
function toggleTab(tabToActivate, tabToDisable, contentToShow, contentToHide) {
  if (tabToDisable.classList.contains(selectedTabClass)) {
    tabToDisable.classList.remove(selectedTabClass);
  }
  if (tabToActivate.classList.contains(selectedTabClass) !== true) {
    tabToActivate.classList.add(selectedTabClass);
  }
  contentToShow.style.display = 'flex';
  contentToHide.style.display = 'none';
}

/**
 * Persist selected departements in localStorage
 */
const updateDeptsInStorage = () => {
  selectedDepts.sort();
  localStorage.setItem(STORAGE_DEPTS_KEY, JSON.stringify(selectedDepts));
};

/**
 * Persist selected etats in localStorage
 */
const updateFilterEtatsInStorage = () => {
  selectedEtats.sort();
  localStorage.setItem(STORAGE_ETATS_KEY, JSON.stringify(selectedEtats));
};

/**
 * Return persisted selected etats
 * @return {array}
 */
export function getSelectedEtats() {
  const etatsFromStorage = localStorage.getItem(STORAGE_ETATS_KEY);
  return (
    etatsFromStorage
      ? JSON.parse(etatsFromStorage)
      : Object.keys(EtatsList.getEtats())
  );
}

/**
 * Remove a selected dept (move it from selected to available)
 */
const unselectDept = function({ target }) {
  if (!target) {
    console.warn('Cannot unselect departement');
    return;
  }
  target.removeEventListener('click', unselectDept);
  target.addEventListener('click', selectDept);
  ListHelper.addElement(deptsAvailableList, target);

  const
    deptNum = target.dataset.dept,
    index = selectedDepts.indexOf(deptNum);

  if (index !== -1) {
    selectedDepts.splice(index, 1);
  }

  updateDeptsInStorage();

  sortList(deptsAvailableList);

  performCount();
  launchResizeDeptsLists();
};

const selectDeptByNumber = (number) => {
  selectDept({
    target: deptsAvailableList.querySelector('li[data-dept="' + number + '"]')
  });
};

/**
 * Add a selected dept (move it from available to selected)
 */
const selectDept = function({ target }) {
  if (!target) {
    console.warn('Cannot select departement');
    return;
  }
  target.removeEventListener('click', selectDept);
  target.addEventListener('click', unselectDept);
  ListHelper.addElement(deptsSelectedList, target);

  let deptNum = target.dataset.dept;
  if (selectedDepts.indexOf(deptNum) === -1) {
    selectedDepts.push(deptNum);
  }

  updateDeptsInStorage();

  sortList(deptsSelectedList);

  performCount();
  launchResizeDeptsLists();
};

/**
 * Empty selected dept list
 */
const emptySelectedDept = () => {
  while (deptsSelectedList.children.length > 0) {
    unselectDept({
      target: deptsSelectedList.children[0],
    });
  }
};

let timerResizeDeptsList;

const launchResizeDeptsLists = () => {
  if (timerResizeDeptsList) {
    clearTimeout(timerResizeDeptsList);
  }
};

/**
 * @param list
 */
const sortList = (list) => {
  ListHelper.sort(list, { attr: 'data-dept' });
};

const etatClassToToggle = 'available-etat';
const toggleAvailable = function({ target }) {
  Utils.toggleCssClass(target, etatClassToToggle);

  // Update list of selected etats (in localStorage too)
  const etatId = parseInt(target.dataset.id, 10),
    isAvailable = target.classList.contains(etatClassToToggle);

  if (isAvailable) {
    // remove from etats selected
    const index = selectedEtats.indexOf(etatId);
    if (index !== -1) {
      selectedEtats.splice(index, 1);
    }
  } else {
    // add to etats selected
    if (selectedEtats.indexOf(etatId) === -1) {
      selectedEtats.push(etatId);
    }
  }
  updateFilterEtatsInStorage();

  performCount();
};

const performCount = function () {
  if (counterValue) {
    counterValue.style.opacity = 0;
  }
  window.setTimeout(showSpinner, 300);

  _performCount();
};

const _performCount = debounce(async function() {
  if (!selectedDepts.length || !selectedEtats.length) {
    hideSpinner();
    showCounter();
    counterValue.innerHTML = '<span>0</span><span>site</span>';
  }
  else {
    let { count, error } = await ServiceHelper.countSites(
      selectedDepts.map((dept) => dept),
      selectedEtats
    );
    hideSpinner();
    if (error) return;

    counterValue.innerHTML = `<span>${count}</span><span>site${count > 1 ? 's' : ''}</span>`;
    showCounter();
  }
}, COUNTER_DELAY);


const showSpinner = () => {
  if (counterValue) {
    counterValue.style.display = 'none';
  }

  if (counterSpinner) {
    counterSpinner.style.display = 'block';
    counterSpinner.style.opacity = 1;
    counterSpinner.classList.add('turn');
  }
};

function hideSpinner() {
  if (counterSpinner) {
    counterSpinner.style.display = 'none';
    counterSpinner.style.opacity = 0;
    counterSpinner.classList.remove('turn');
  }
}
function showCounter() {
  if (counterValue) {
    counterValue.style.display = 'flex';
    counterValue.style.opacity = 1;
  }
}

const btnValiderClick = async () => {
  if (selectedDepts.length && selectedEtats.length) {
    btnValider.disabled = true;

    PubSub.publish(Events.GET_SITES, { depts: selectedDepts, etats: selectedEtats, });

    // Zoom on first selected departement
    const firstSelectedDeptNum = selectedDepts[0];
    if (firstSelectedDeptNum === POSTAL_CODE_FOR_FOREIGN_COUNTRIES) {
      // Special foreign countries case
      PubSub.publish(Events.MAP_ZOOM_TO, 6);
      return;
    }

    const firstSelectedDept = Departements.find(d => d.num === firstSelectedDeptNum);
    if (firstSelectedDept) {
      // Focus the map on the first selected departement
      const result = await GeocoderHelper.performGeocode({
        address: `${firstSelectedDept.nom}, France`,
      });
      if (result) {
        PubSub.publish(Events.MAP_PAN_TO, { location: result.geometry.location });
      }
    }
  } else {
    PubSub.publish(Events.REFRESH_SITES, []);
  }
};
