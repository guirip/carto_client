
import 'style/alertify.override.scss';

/**
 * https://alertifyjs.com/notifier/delay.html
 * @param {object} opts
 */
function setAlertifyProperties(opts) {
  alertify.set(
    'notifier',
    'position',
    opts && opts.position ? opts.position : 'top-center'
  );
  alertify.set(
    'notifier',
    'delay',
    opts && typeof opts.delay === 'number' ? opts.delay : 3
  );
}

/**
 * Display an quick information message
 * @param  {string} message
 * @param  {object} opts
 */
export function info(message, opts) {
  setAlertifyProperties(opts);
  alertify.success(message);
}

/**
 * Display an error message, then a user action is required to close it.
 * @param  e
 * @param  {object} opts
 */
export function error(e, opts) {
  const text = filterMultiLineMessage(e);
  setAlertifyProperties(Object.assign({}, {delay: 8}, opts));
  alertify.error(text);
}

/**
 * Display an warning message
 * @param  messages
 * @param  {object} opts
 */
export function warn(messages, opts) {
  if (!opts) {
    opts = {};
  }
  if (typeof opts.delay !== 'number') {
    opts.delay = 4;
  }
  setAlertifyProperties(opts);
  alertify.warning(filterMultiLineMessage(messages));
}

/**
 * doc : https://alertifyjs.com/confirm.html
 *
 * Show a confirm dialog
 * @param message
 * @param { Function } okCallback
 * @param { Function } cancelCallback (optional)
 * @param { String } position : Overrides default position
 */
export function confirm(message, okCallback, cancelCallback, position) {
  setAlertifyProperties();
  alertify
    .confirm()
    .setting({
      closable: false,
      transitionOff: true,
      message,
      title: '',
      onok: okCallback,
      oncancel: cancelCallback,
    })
    .show();
}

/**
 * @param data
 */
const filterMultiLineMessage = (data) => {
  if (typeof data === 'object') {
    return Object.keys(data)
      .map((key) => data[key])
      .join('\n');
  } else {
    return data;
  }
};

/**
 * @param {Function} cb: callback
 * @param {String}   name
 */
const _performCallback = function (cb, name) {
  if (cb) {
    if (typeof cb === 'function') {
      cb();
    } else {
      console.error(
        'The confirm dialog ' +
          name +
          ' callback is not a function: ' +
          typeof cb
      );
    }
  }
};
