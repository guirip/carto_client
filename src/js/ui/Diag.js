import PubSub from 'pubsub-js';

import * as Utils from 'modules/Utils';
import * as Events from 'modules/Events';
import * as ServiceHelper from 'modules/ServiceHelper';

import * as Messages from './Messages';

import diagTpl from 'templates/diag.html';
import diagContentTpl from 'templates/diagContent.html';

/**
 * Diag module
 */

// Local variables
const
  containerId = 'diag-container',
  okClass = 'diag-ok',
  koClass = 'diag-ko',
  DETAILS_DISPLAY = {
    visible: 'flex',
    hidden: 'none',
  },
  LIGHT_DISPLAY = {
    visible: 'block',
    hidden: 'none',
  };

let container,
  mainTplApplied = false,
  light,
  details,
  refresh,
  requestRunning = false,
  globalStatus = false;

export const events = {
  IS_OK: 'DIAG_IS_OK',
};

/**
 * Template data
 */
const tplData = {
  getMainStatusClass: function () {
    return globalStatus ? okClass : koClass;
  },
  getStatusClass: function () {
    return this.status == 'OK' ? okClass : koClass;
  },
  getWsMessage: function () {
    return this.status != 'OK' ? 'Serveur de push WebSocket KO' : this;
  },
};

export async function refreshStatus() {
  startSpinner();
  const { data, error } = await ServiceHelper.getDiag();
  setTimeout(stopSpinner, 400);

  if (error) return;

  const autoToggle = (
    details
    && details.style.display === DETAILS_DISPLAY.visible
  );

  const dbStatusOk = data.DB.status == 'OK'
  const wsStatusOk = data.WS.status == 'OK'
  globalStatus = dbStatusOk && wsStatusOk;

  tplData.diag = data;

  if (!mainTplApplied) {
    container.innerHTML = diagTpl(tplData, true);

    light = container.querySelector('.diag-light');
    light.addEventListener('click', showDetails);
    details = container.querySelector('.diag-details');

    refresh = details.querySelector('.diag-refresh');
    refresh.addEventListener('click', refreshStatus);

    mainTplApplied = true;
  } else {
    // Update light
    light.classList.remove(okClass);
    light.classList.remove(koClass);
    light.classList.add(tplData.getMainStatusClass());
  }
  container.querySelector('ul').innerHTML = diagContentTpl(tplData, true);

  // Automatically re-open if details were visible
  if (autoToggle) {
    showDetails();
  }

  if (dbStatusOk) {
    PubSub.publish(events.IS_OK);

    if (!wsStatusOk) {
      Messages.warn(
        'Serveur temps réel non joignable !<br><br>Envoie moi un message pour me prévenir stp 🙏<br><br>D\'ici là il te faudra rafraichir la page pour voir tes modifications.',
        { delay: 6 },
      );
    }
  } else {
    handleDiagKO(data);
  }
}

/**
 * Handle default behaviour when diag is KO
 * @param { Object } data : diag service response
 */
async function handleDiagKO(data) {
  showDetails();
  Messages.error('<div style="font-size: 1.4em">Serveur hors-service 😨<br>Merci de me prévenir</div>');

  // TODO
  /*await ServiceHelper.sendEmail(
    '/!\\ Urgent : Carto hors-service /!\\',
    JSON.stringify(data),
    function () {
      // success
      Messages.error(
        "Serveur hors-service.<br>Un email a été envoyé à l'administrateur.<br>Merci de revenir plus tard."
      );
    },
    function () {
      // failure
      Messages.error("Serveur hors-service. Merci de relayer l'information.");
    }
  );*/
}

/**
 * Initialize module
 */
async function init() {
  await import('style/diag.scss');

  container = document.createElement('div');
  container.id = containerId;
  container.classList.add('popup');
  document.body.appendChild(container);

  await refreshStatus();
}

const startSpinner = () => {
  requestRunning = true;
  if (refresh) {
    refresh.style.opacity = 0.5;
    refresh.classList.add('turn');
  }
};

const stopSpinner = () => {
  requestRunning = false;
  if (refresh) {
    refresh.style.opacity = 1;
    refresh.classList.remove('turn');
  }
};

export const showDetails = () => {
  light.style.display = LIGHT_DISPLAY.hidden;
  details.style.display = DETAILS_DISPLAY.visible;
};

export const hideDetails = () => {
  details.style.display = DETAILS_DISPLAY.hidden;
  light.style.display = LIGHT_DISPLAY.visible;
};

/**
 * Hide the details when user click outside of it
 * @param event
 * @param target
 */
const checkClickedOut = (event, target) => {
  if (
    details &&
    details.style.display === DETAILS_DISPLAY.visible &&
    target.id != containerId &&
    Utils.findParentNode(target, el => el.id === containerId) === null
  ) {
    hideDetails();
  }
};
PubSub.subscribe(Events.BODY_CLICKED, checkClickedOut);

Utils.executeOnDomReady(init);
