import PubSub from 'pubsub-js';

import * as Events from 'modules/Events';
import * as Utils from 'modules/Utils';

import * as EtatsList from './EtatsList';
import * as Filter from './Filter';

import etatsListTpl from 'templates/etatsList.html';
import aroundTpl from 'templates/around.html';

let
  moduleInitalized = false,
  container,
  etatsListEl,
  selectedEtats = [],
  currentDistanceEl,
  selectedDistance,
  currentLat,
  currentLng;

export const events = {
  SHOW: 'AROUND_SHOW',
  HIDE: 'AROUND_HIDE',
};

const STORAGE_AROUND_ETATS_KEY = 'around-etats';
const STORAGE_AROUND_DISTANCE_KEY = 'around-dist';
const AROUND_DIST_STEP = 5;
const AROUND_DIST_MIN = 10;
const AROUND_DIST_MAX = 150;
const AROUND_DEFAULT_DISTANCE = 40;

const tplAroundData = {
  etatsLabel: 'Etats',
  distanceLabel: 'Distance max',
  distanceStep: AROUND_DIST_STEP,
  distanceMin: AROUND_DIST_MIN,
  distanceMax: AROUND_DIST_MAX,
  validerLabel: 'Afficher',
  validerTitle: 'Afficher les lieux correspondants',
};


EtatsList.doWhenReady(() => {
  tplAroundData.htmlEtatsList = etatsListTpl({
    etats: EtatsList.getEtats(),
  });
});


/**
 * Show the popup, initializing the module first if needed
 */
async function show(eventName, { lat, lng }) {
  if (!moduleInitalized) {
    await import('style/around.scss');
    moduleInitalized = true;
  }

  currentLat = lat;
  currentLng = lng;

  if (!container) {
    const storedDistance = localStorage.getItem(STORAGE_AROUND_DISTANCE_KEY);
    selectedDistance = (
      storedDistance
        ? parseInt(storedDistance, 10)
        : AROUND_DEFAULT_DISTANCE
    );

    tplAroundData.distanceValue = selectedDistance;

    // Apply template
    container = Utils.tplToDom(aroundTpl, tplAroundData);

    [
      ['.popup-close', hide],
      ['.btn-valider', onAroundSubmit],
    ]
    .forEach(([ selector, handler ]) => {
      const el = container.querySelector(selector);
      if (!el) {
        console.warn('[around] cannot match selector: '+selector);
        return;
      }
      container.querySelector(selector).addEventListener('click', handler);
    });

    // init etats click handlers
    etatsListEl = container.querySelector('.etats-list');
    const lis = etatsListEl.querySelectorAll('li');
    for (const li of lis) {
      li.addEventListener('click', toggleAroundEtatAvailable);
    }

    // init selected etats
    const etatsFromStorage = localStorage.getItem(STORAGE_AROUND_ETATS_KEY);
    const etatsToSelect = (
      etatsFromStorage
        ? JSON.parse(etatsFromStorage)
        : Filter.getSelectedEtats()
    );
    etatsToSelect.forEach((etatId) => {
      const li = etatsListEl.querySelector('li[data-id="' + etatId + '"]');
      toggleAroundEtatAvailable({ target: li });
    });

    // distance
    container.querySelector('#a-dist-range').addEventListener('input', onDistanceChange);
    currentDistanceEl = container.querySelector('#a-dist-current');

    // add container to DOM
    document.body.appendChild(container);
  }

  container.style.display = 'flex';
  container.style.opacity = 1;
}
PubSub.subscribe(events.SHOW, show);


/**
 * Hide the popup
 */
function hide() {
  if (container) {
    Utils.closeModule(container);
  }
};
PubSub.subscribe(events.HIDE, hide);


const etatClassToToggle = 'available-etat';
const toggleAroundEtatAvailable = function({ target }) {
  Utils.toggleCssClass(target, etatClassToToggle);

  // Update list of selected etats (in localStorage too)
  const etatId = parseInt(target.dataset.id, 10),
    isAvailable = target.classList.contains(etatClassToToggle);

  if (isAvailable) {
    // remove from etats selected
    const index = selectedEtats.indexOf(etatId);
    if (index !== -1) {
      selectedEtats.splice(index, 1);
    }
  } else {
    // add to etats selected
    if (selectedEtats.indexOf(etatId) === -1) {
      selectedEtats.push(etatId);
    }
  }
  updateAroundEtatsInStorage();

  // performCount();
};

/**
 * Persist around selected etats in localStorage
 */
function updateAroundEtatsInStorage() {
  selectedEtats.sort();
  localStorage.setItem(STORAGE_AROUND_ETATS_KEY, JSON.stringify(selectedEtats));
};

function onDistanceChange({ target }) {
  selectedDistance = parseInt(target.value, 10);
  currentDistanceEl.textContent = selectedDistance;
  localStorage.setItem(STORAGE_AROUND_DISTANCE_KEY, selectedDistance);
}

/**
 * Handle submission
 */
async function onAroundSubmit() {
  const etats = selectedEtats;
  const distance = selectedDistance;

  PubSub.publish(Events.GET_SITES_AROUND, {
    lat: currentLat,
    lng: currentLng,
    distance,
    etats,
    focus: false,
  });
}

// TODO
// PubSub.subscribe(Events.BODY_CLICKED, bodyClicked);
