import PubSub from 'pubsub-js';

import * as Events from 'modules/Events';
import * as Utils from 'modules/Utils';
import * as GeocoderHelper from 'modules/GeocoderHelper';

import * as Messages from './Messages';

import rechercheTpl from 'templates/recherche.html';

/**
 * Recherche module
 */

// Local variables
const
  focusedClass = 'focused',
  frToggleClass = 'fr-enabled',
  regexpGeodesique = new RegExp('(-?[0-9.]+)\\s*,\\s*(-?[0-9.]+)'),
  sexaPart = '([0-9]*)°\\s*([0-9]*)\'\\s*([0-9.]*)"\\s*',
  regexpSexagesimal = new RegExp(
    sexaPart + '([NS])\\s*,?\\s*' + sexaPart + '([EOW])'
  );

let container,
  containerId,
  input,
  frToggleBtn,
  frEnabled,
  tplData = {
    placeholder: "Recherche d'adresse, commune, etc",
    franceLabel: 'en France',
    rechercheFranceLabel: 'Recherche en France',
    rechercheMondialeLabel: 'Recherche mondiale',
  };

/**
 * Initialise module
 */
async function init() {
  await import('style/recherche.scss');

  container = Utils.tplToDom(rechercheTpl, tplData);
  containerId = container.id;

  input = container.querySelector('input');
  input.addEventListener('click', rechercheClickHandler);
  input.addEventListener('keyup', rechercheKeyupHandler);

  frToggleBtn = container.querySelector('.rech-france-toggle');
  frToggleBtn.addEventListener('click', toggleFrBtn);
  updateFrToggle();

  Utils.appendAndShowModule(container);
}

/**
 * Called when input is clicked
 */
const rechercheClickHandler = () => {
  container.classList.add(focusedClass);
  input.select();
};

/**
 * Called when a key is pressed in input
 *
 * @param e
 */
const rechercheKeyupHandler = function ({ keyCode, target }) {
  if (keyCode == 13) {
    performRecherche(target.value, target);
  }
};

/**
 * Perform search
 *
 * @param adresse
 * @param target
 */
const performRecherche = async (adresse, target) => {
  if (!adresse) {
    Messages.warn('Saisis quelque-chose face de nouille.', target);
  } else {
    let opts = {};

    // Coordonnées en système geodesique
    if (regexpGeodesique.test(adresse)) {
      opts.location = new google.maps.LatLng(
        adresse.split(',')[0],
        adresse.split(',')[1]
      );
    }
    // Coordonnées en système sexagesimal
    else if (regexpSexagesimal.test(adresse)) {
      const
        parts = regexpSexagesimal.exec(adresse),
        dmsX = {
          d: parseInt(parts[1], 10),
          m: parseInt(parts[2], 10),
          s: parseFloat(parts[3]),
          signe: parts[4] == 'N' ? 1 : -1,
        },
        dmsY = {
          d: parseInt(parts[5], 10),
          m: parseInt(parts[6], 10),
          s: parseFloat(parts[7]),
          signe: parts[8] == 'E' ? 1 : -1,
        };

      opts.location = new google.maps.LatLng(dmsToDeg(dmsX), dmsToDeg(dmsY));
    } else if (frEnabled && !/ France$/.exec(adresse)) {
      // Force France
      opts.address = adresse + ', France';
    } else {
      opts.address = adresse;
    }

    const result = await GeocoderHelper.performGeocode(opts);

    input.value = result.formatted_address;
    PubSub.publish(Events.MAP_PAN_TO, {
      location: result.geometry.location,
      zoom: 12
    });
  }
};

/**
 * Handle click on toggleFr button
 */
const toggleFrBtn = () => {
  Utils.toggleCssClass(frToggleBtn, frToggleClass);
  updateFrToggle();
};

/**
 * Update toggle value and title attribute
 */
const updateFrToggle = () => {
  if (frToggleBtn.classList.contains(frToggleClass)) {
    frEnabled = true;
    frToggleBtn.title = tplData.rechercheFranceLabel;
  } else {
    frEnabled = false;
    frToggleBtn.title = tplData.rechercheMondialeLabel;
  }
};

/**
 * Unfocus input when user click outside of it
 * @param event
 * @param target
 */
const checkClickedOut = (event, target) => {
  if (
    input &&
    input.classList &&
    container.classList.contains(focusedClass) &&
    target.id != containerId &&
    Utils.findParentNode(target, el => el.id === containerId) === null
  ) {
    container.classList.remove(focusedClass);
  }
};
PubSub.subscribe(Events.BODY_CLICKED, checkClickedOut);

/**
 * Convert DMS to degres
 * @param  { object } dms as { d: ?, m: ?, s: ? }
 * @return { float }
 */
const dmsToDeg = (dms) => {
  return dms.signe * (Math.abs(dms.d) + dms.m / 60 + dms.s / 3600);
};

Utils.executeOnDomReady(init);
