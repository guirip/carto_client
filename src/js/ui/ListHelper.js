
/**
 * @param ul
 * @param li
 */
export const addElement = (ul, li) => {
  ul.appendChild(li);
};

/**
 * @param ulEl
 * @param options (optional, default: asc)
 */
export const sort = (ulEl, options) => {
  const nodes = ulEl.childNodes;

  // Convert to array
  const liEls = [];
  for (const i in nodes) {
    if (nodes[i].nodeType == 1) {
      // get rid of the whitespace text nodes
      liEls.push(nodes[i]);
    }
  }

  // Sort
  liEls.sort((liEl1, liEl2) => liEl1.dataset.dept.localeCompare(liEl2.dataset.dept));

  // Refresh ul
  ulEl.innerHTML = '';
  liEls.forEach((liEl) => {
    ulEl.appendChild(liEl);
  });
};
