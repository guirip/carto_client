import PubSub from 'pubsub-js';

import * as Events from 'modules/Events';
import * as Utils from 'modules/Utils';

import siteLinkTpl from 'templates/siteLink.html';

import 'style/site-link.scss';

/**
 * SiteLink module
 */

// Local variables
const siteLinkId = 'site-link';
const DISPLAY = {
  visible: 'flex',
  hidden: 'none',
};
let siteLink;

export const LINK_PARAM_NAME = 'id';

const selectLinkText = function () {
  siteLink.querySelector('input').select();
};

/**
 * Show a http link to this site
 * @param id
 * @return {string} direct link to current site
 */
export const linkOnSite = function (id) {
  if (id) {
    if (!siteLink) {
      siteLink = Utils.tplToDom(siteLinkTpl);
      siteLink.addEventListener('click', selectLinkText);
      document.body.appendChild(siteLink);
    }

    const { protocol, host, pathname } = window.location;
    const link = `${protocol}//${host}${pathname}?${LINK_PARAM_NAME}=${id}`;
    siteLink.querySelector('input').value = link;

    siteLink.style.display = DISPLAY.visible;

    setTimeout(selectLinkText, 10);
    return link;
  }
};

/**
 * Hide the link popup when user click outside of it
 * @param event
 * @param target
 */
const checkClickedOut = function (event, target) {
  if (
    target.classList.contains('fiche-btn-link') !== true &&
    siteLink &&
    siteLink.style.display === DISPLAY.visible &&
    target.id != siteLinkId &&
    Utils.findParentNode(target, el => el.id === siteLinkId) === null
  ) {
    siteLink.style.display = DISPLAY.hidden;
  }
};
PubSub.subscribe(Events.BODY_CLICKED, checkClickedOut);
