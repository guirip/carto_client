import PubSub from 'pubsub-js';

import * as Utils from 'modules/Utils';
import * as Events from 'modules/Events';
import * as Context from 'modules/Context';
import * as ServiceHelper from 'modules/ServiceHelper';
import * as DiaryFilterSettings from './DiaryFilterSettings';

import * as Diag from './Diag';
import * as EtatsList from './EtatsList';

import diaryTpl from 'templates/diary.html';
import waiterTpl from 'templates/waiter.html';
import logWsNoEventTpl from 'templates/logWsNoEvent.html';

import logWsEvent_S_CR_Tpl from 'templates/logWsEvent_S_CR.html';
import logWsEvent_S_PU_Tpl from 'templates/logWsEvent_S_PU.html';
import logWsEvent_S_EU_Tpl from 'templates/logWsEvent_S_EU.html';
import logWsEvent_R_CR_Tpl from 'templates/logWsEvent_R_CR.html';
import logWsEvent_P_ADTpl from 'templates/logWsEvent_P_AD.html';


/**
 * Diary module
 */

// Local constants
const
  UNFOLDED_WIDTH = 850,
  streetViewClass = 'street-view-mode',
  filterClass = 'pressed',
  DIARY_DISPLAY = {
    visible: 'flex',
    hidden: 'none',
  };

// Local variables
let diaryId,
  diary,
  topLineEl,
  closeBtnEl,
  waiter,
  compteur,
  diaryList,
  initialEntriesFetched = false,
  loggedEventsCount,
  foldedWidth,
  disableClickedOutCheck = false;

const eventTemplates = {
  [Events.SITE_CREATED]: logWsEvent_S_CR_Tpl,
  [Events.SITE_POSITION_UPDATED]: logWsEvent_S_PU_Tpl,
  [Events.SITE_ETAT_UPDATED]: logWsEvent_S_EU_Tpl,
  [Events.REMARQUE_CREATED]: logWsEvent_R_CR_Tpl,
  [Events.PHOTO_ADDED]: logWsEvent_P_ADTpl,
};

/**
 * Template diary data
 */
const diaryTplData = {
    title: 'Journal',
    filter: {
      event1Label: 'Ajouts',
      event1Title: 'Voir les ajouts de sites',
      event2Label: 'Déplacements',
      event2Title: 'Voir les déplacements de sites',
      event3Label: 'Etats',
      event3Title: "Voir les modification d'état",
      event4Label: 'Commentaires',
      event4Title: 'Voir les ajouts de commentaires',
      buttons: DiaryFilterSettings.get(),
    },
    compteurTitle: 'Nouvelles actions des autres utilisateurs',
    count: 0,
    arrowTitle: 'Afficher/masquer le contenu du journal',
    limitedDiary:
      'Pour une question de performance la taille du journal est limitée.',
  },
  noEventTplData = {
    label: 'Aucun résultat',
  };

export const events = {
  SHOW: 'DIARY_SHOW',
};

/**
 * Template event data
 */
const logEventTplData = {};

const getEtatLibelle = (etat) => {
  return EtatsList.getEtat(etat).libelle;
};

const getDateLabel = function (date) {
  return Utils.formatDate(date);
};

/**
 * Initialize module
 */
async function init() {
  if (!diary) {
    await import('style/diary.scss');

    diary = Utils.tplToDom(diaryTpl, diaryTplData);
    diaryId = diary.id;

    Utils.appendAndShowModule(diary);

    // Buttons
    topLineEl = diary.querySelector('.diary-topline');
    closeBtnEl = topLineEl.querySelector('.popup-close');
    if (closeBtnEl) {
      closeBtnEl.addEventListener('click', fold);
    }

    // Button not visible anymore
    // btnFold = diary.querySelector('.btn-arrow');
    // diary.addEventListener('click', '.btn-up.active', fold);
    // diary.addEventListener('click', '.btn-down.active', unfold);

    // Button not visible anymore
    // filter = diary.find('.diary-filter');
    // filter.addEventListener('click', 'span', updateFilter);

    // List
    diaryList = document.querySelector(`#${diaryId} > ul`);

    foldedWidth = diary.offsetWidth;
  }
}

const getEntries = () => {
  initialEntriesFetched = false;

  // Show spinner
  if (!waiter) {
    // init spinner once
    waiter = waiterTpl();
  }
  diaryList.innerHTML = waiter;

  EtatsList.doWhenReady(fetchContent);
}

async function fetchContent() {
  const { data:dtos, error } = await ServiceHelper.getDiaryEntries();
  if (error) return;

  loggedEventsCount = 0;
  diaryList.innerHTML = '';
  diaryList.style.maxHeight = 'none';

  const diaryLimitedEl = diary.querySelector('#diary-limited');
  if (diaryLimitedEl) {
    diaryLimitedEl.style.display = 'flex';
  }

  if (!dtos.length) {
    diaryList.appendChild(Utils.tplToDom(logWsNoEventTpl, noEventTplData));
  } else {
    dtos.forEach(function(dto) {
      logEvent(getEventTypedDto(dto), {
        insertMethod: 'append',
        init: true,
      });
    });

    // Apply current filter
    Object.keys(DiaryFilterSettings.get()).forEach(type => {
      applyFilter(type);
    });
  }
  initialEntriesFetched = true;
}
PubSub.subscribe(Diag.events.IS_OK, getEntries);


/**
 * @param dto
 */
const getEventTypedDto = (_dto) => {
  let dto = {
    type: _dto.TYPE,
    author: _dto.AUTHOR,
    date: getDateLabel(_dto.DATE),
    data: {
      SITE_ID: _dto.SITE_ID,
      ADRESSE: _dto.ADRESSE,
      ETAT: _dto.ETAT,
      DEPARTEMENT: _dto.DEPARTEMENT,
      PAYS: _dto.PAYS,
      NOM_USUEL: _dto.NOM_USUEL,
      DATE: _dto.DATE,
      REMARQUE: _dto.REMARQUE,
    },
  };

  switch (_dto.TYPE) {
    case Events.SITE_ETAT_UPDATED:
      dto.etatLibelle = getEtatLibelle(_dto.ETAT);
      break;

    case Events.REMARQUE_CREATED:
      dto.remarqueWithoutHtml = Utils.removeHtmlTags(_dto.REMARQUE);
      break;
  }

  return dto;
};

/**
 * Add an event to diary
 * @param data
 * @param opts (optional)
 */
const logEvent = (event, opts) => {
  // Filter some events
  if (eventTemplates[event.type]) {
    logEventTplData.event = event;
    logEventTplData.isUser = event.author == Context.getAuthor();
    logEventTplData.area = event.data.DEPARTEMENT || event.data.PAYS || '?';

    if (!event.date) {
      // Fallback when event.date is missing
      let dateIso;
      switch (event.type) {
        case Events.SITE_CREATED:
          dateIso = event.data.CREATION_DATE;
          break;
        case Events.SITE_POSITION_UPDATED:
          dateIso = event.data.POSITION_DATE;
          break;
        case Events.SITE_ETAT_UPDATED:
          dateIso = event.data.ETAT_DATE;
          break;
        case Events.REMARQUE_CREATED:
          dateIso = event.data.DATE;
          break;
        // case Events.PHOTO_ADDED: break; // TODO
      }
      if (dateIso) {
        event.date = getDateLabel(new Date(dateIso));
      }
    }

    const li = Utils.tplToDom(eventTemplates[event.type], logEventTplData);
    li.addEventListener('click', logClickHandler);

    // Add new element
    if (opts && opts.insertMethod == 'append') {
      diaryList.appendChild(li);
    } else {
      diaryList.insertBefore(li, diaryList.children[0]);
      if (!logEventTplData.isUser && compteur) {
        setCompteurValue(parseInt(compteur.dataset.value, 10) + 1);
      }
    }
    loggedEventsCount++;

    // Pop oldest element - Does not seem useful
    /*if ((!opts || !opts.init) && loggedEventsCount > 50) {
      diaryList.querySelector('li:last-child').remove();
    }*/
  }
};
/**
 * Log pushed events by prepending then on diary's first page
 * @param wsEventName
 * @param data
 */
const logPushedEvent = (wsEventName, data) => {
  if (
    initialEntriesFetched &&
    // and filter is active for this type of event
    DiaryFilterSettings.isActive(data.type)
  ) {
    logEvent(data);
  }
};
PubSub.subscribe(Events.PUSH_EVENT, logPushedEvent);

/**
 * Called when an entry is clicked
 */
const logClickHandler = function({ target }) {
  const { id, eventType } = target.dataset;

  if (eventType === Events.REMARQUE_CREATED && Utils.isMobileDevice() !== true) {
    // Focus + open fiche
    PubSub.publish(Events.SITE_FOCUS, id);
  } else {
    // Only focus
    PubSub.publish(Events.SITE_ZOOM, id);
  }

  fold();
};

/**
 * Update compteur
 * @param value
 */
const setCompteurValue = (value) => {
  compteur.innerHTML = value;
  compteur.dataset.value = value;
};

// TODO : fold & foldNextStep via CSS transition

const fold = () => {
  diary.style.display = DIARY_DISPLAY.hidden;
};
/*const fold = () => {
    // cancel eventual unfolding
    // Utils.killTween(unfoldingTween1);
    // Utils.killTween(unfoldingTween2);

    // foldingTween1 = TweenLite.to(diaryList, 0.65, {
    //     'flex-basis': 0,
    //     onComplete  : foldNextStep
    // });
    diaryList.style.flexBasis = 0; // TODO: + 'px' ?
    foldNextStep();
};
const foldNextStep = () => {
    diaryList.style.display = 'none';
    btnFold.classList.remove('btn-up');
    btnFold.classList.add('btn-down');

    // foldingTween2 = TweenLite.to(diary, 0.35, {
    //     width: foldedWidth
    // });
    diary.style.width = foldedWidth; // TODO: + 'px' ?
    hideFilter();
    setCompteurValue(0);
};*/
// TODO : unfold & unfoldNextStep via CSS transition

const unfold = () => {
  diary.style.display = DIARY_DISPLAY.visible;
  diary.style.width = UNFOLDED_WIDTH;
  diaryList.style.flexBasis = '1000px';

  disableClickedOutCheck = true;
  window.setTimeout(() => {
    disableClickedOutCheck = false;
  }, 500);
};
PubSub.subscribe(events.SHOW, unfold);
/*const unfold = () => {
    // cancel eventual folding
    // Utils.killTween(foldingTween1);
    // Utils.killTween(foldingTween2);

    // unfoldingTween1 = TweenLite.to(diary, 0.35, {
    //     width: UNFOLDED_WIDTH,
    //     onComplete: unfoldNextStep
    // });
    diary.style.width = UNFOLDED_WIDTH; // TODO: + 'px' ?
    showFilter();
    unfoldNextStep();
};
const unfoldNextStep = () => {
    btnFold.classList.remove('btn-down');
    btnFold.classList.add('btn-up');
    diaryList.style.display = 'block';
    // unfoldingTween2 = TweenLite.to(diaryList, 0.65, {
    //     'flex-basis': 1000
    // });
    diaryList.style.flexBasis = '1000px';
    setCompteurValue(0);
};*/

/**
 * Update filter when a button is clicked
 */
const updateFilter = (event) => {
  let isPressed = false,
    btnEl = event.target;

  // Toggle css class
  if (btnEl.classList.contains(filterClass)) {
    btnEl.classList.remove(filterClass);
  } else {
    btnEl.classList.add(filterClass);
    isPressed = true;
  }

  // Update state
  const type = parseInt(btnEl.dataset.event, 10);
  DiaryFilterSettings.update(type, isPressed ? 1 : 0);

  // Hide or show corresponding <li> elements
  applyFilter(type);
};

/**
 *
 * Hide or show corresponding <li> elements
 * @param  {string} type
 */
const applyFilter = (type) => {
  diaryList
    .querySelectorAll("[data-event-type='" + type + "']")
    .forEach((liEl) => {
      if (DiaryFilterSettings.get()[type] === 1) {
        // unfilter
        liEl.style.display = 'flex';
      } else {
        // filter
        liEl.style.display = 'none';
      }
    });
};

/*
 * Toggle filter functions
 *
const showFilter = () => {
  filter.style.display = 'inline-block';
};
const hideFilter = () => {
  filter.style.display = 'none';
};*/

/**
 * Hide the list when user click outside of it
 * @param event
 * @param target
 */
const checkClickedOut = (event, target) => {
  if (
    !disableClickedOutCheck &&
    diaryList &&
    diaryList.style.display === DIARY_DISPLAY.visible &&
    target.id != diaryId &&
    Utils.findParentNode(target, el => el.id === diaryId) === null
  ) {
    fold();
  }
};
PubSub.subscribe(Events.BODY_CLICKED, checkClickedOut);

/**
 * Adjust distance from top. (Fix maps native reverse address overlay)
 */
const streetViewEnabledHandler = () => {
  diary.classList.add(streetViewClass);
};
PubSub.subscribe(Events.MAP_STREETV_OPEN, streetViewEnabledHandler);

/**
 * Set back normal distance from top
 */
const streetViewDisabledHandler = () => {
  diary.classList.remove(streetViewClass);
};
PubSub.subscribe(Events.MAP_STREETV_CLOSE, streetViewDisabledHandler);

Utils.executeOnDomReady(init);
