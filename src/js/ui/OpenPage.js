import PubSub from 'pubsub-js';

import * as Events from 'modules/Events';
import * as Utils from 'modules/Utils';

import openTpl from 'templates/open.html';

import 'style/open.scss';

/**
 * OpenPage module
 */

const streetViewClass = 'street-view-mode';
const buttons = [];

/**
 * Add a button to open an html page
 * @param  {object} opts
 */
export const addButton = (opts) => {
  // Apply template
  const btn = Utils.tplToDom(openTpl, opts.tplData);

  // Add click handler
  btn.addEventListener('click', function () {
    window.open(opts.page);
  });

  buttons.push(btn);
  Utils.appendAndShowModule(btn);
};

/**
 * Adjust position
 */
const streetViewEnabledHandler = () => {
  for (const index in buttons) {
    buttons[index].classList.add(streetViewClass);
  }
};
PubSub.subscribe(Events.MAP_STREETV_OPEN, streetViewEnabledHandler);

/**
 * Revert position to normal
 */
const streetViewDisabledHandler = () => {
  for (const index in buttons) {
    buttons[index].classList.remove(streetViewClass);
  }
};
PubSub.subscribe(Events.MAP_STREETV_CLOSE, streetViewDisabledHandler);
