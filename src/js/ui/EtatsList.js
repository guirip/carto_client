import PubSub from 'pubsub-js';

import * as Utils from 'modules/Utils';
import * as Events from 'modules/Events';
import * as ServiceHelper from 'modules/ServiceHelper';

import * as Diag from './Diag';

import etatsListTpl from 'templates/etatsList.html';

import 'style/etats.scss';

/**
 * EtatsList module
 */

// Local variables
const
  etatsListId = 'etats-list',
  listOffsetTop = 105,
  availableEtatClass = 'available-etat';

let ready = false,
  isVisible = false,
  etatsContainerEl,
  resizeTimeout,
  etats,
  callback;

export const events = {
  OPEN: 'ETAT_OPEN',
  CLOSE: 'ETAT_CLOSE',
};

/**
 * Fetch etats
 */
async function initEtats() {
  const { data, error } = await ServiceHelper.getEtats();
  if (error) return;

  etats = data;
  ready = true;
}
PubSub.subscribe(Diag.events.IS_OK, initEtats);

/**
 * Return etats
 *
 */
export const getEtats = () => {
  return etats;
};

/**
 * Return etat
 *
 * @param id
 */
export const getEtat = (id) => {
  if (typeof id === 'string') {
    id = parseInt(id, 10);
  }

  return etats.find((etat) => etat.id === id);
};

/**
 * Populate and show the list of etats
 * @param event
 * @param data
 */
const populateAndShow = (event, data) => {
  if (!etatsContainerEl) {
    etatsContainerEl = document.getElementById('etats-container');
    etatsContainerEl.innerHTML = etatsListTpl({ etats: etats });

    const lis = etatsContainerEl.querySelectorAll('li.' + availableEtatClass);
    for (const li of lis) {
      li.addEventListener('click', selectedEtatHandler);
    }
  }

  // Update etats state
  etatsContainerEl.querySelectorAll('li').forEach((el) => {
    el.classList.add(availableEtatClass);
  });
  etatsContainerEl
    .querySelectorAll('li[data-id="' + data.etat + '"]')
    .forEach((el) => {
      el.classList.remove(availableEtatClass);
    });

  etatsContainerEl.style.maxHeight = window.innerHeight - 60 + 'px';

  show();
  callback = data.callback;
};
PubSub.subscribe(events.OPEN, populateAndShow);

/**
 * Show the list
 */
function show() {
  if (etatsContainerEl) {
    Utils.showPopup(etatsContainerEl);
    isVisible = true;
  }
}

/**
 * Hide the list
 */
export const hide = () => {
  if (etatsContainerEl) {
    Utils.hidePopup(etatsContainerEl);
    isVisible = false;
  }
};
PubSub.subscribe(events.CLOSE, hide);

/**
 * Called when an etat has been clicked
 *
 * @param e
 */
const selectedEtatHandler = function (e) {
  e.stopPropagation();
  callback(e.target.dataset.id);
  hide();
};

/**
 * Hide the list when user click outside of it
 * @param event
 * @param target
 */
const onBodyClicked = (event, target) => {
  if (etatsContainerEl &&
      isVisible &&
      Utils.findParentNode(target, el => el.id === etatsListId) === null) {
    hide();
  }

  /*
  if (
    $etatsContainerEl &&
    $etatsContainerEl.is(':visible') &&
    $target.attr('id') != etatsListId &&
    !$target.parents('#' + etatsListId).length
  ) {
    etatsContainerEl.style.display = 'none';
  }*/
};
PubSub.subscribe(Events.BODY_CLICKED, onBodyClicked);

export const isReady = () => {
  return ready === true;
};

/**
 * @param callback
 */
export const doWhenReady = (callback) => {
  if (isReady()) {
    callback();
  } else {
    setTimeout(doWhenReady, 400, callback);
  }
};

const handleWindowResize = () => {
  if (etatsContainerEl) {
    if (resizeTimeout) {
      clearTimeout(resizeTimeout);
    }
    resizeTimeout = setTimeout(performResize, 200);
  }
};
const performResize = () => {
  etatsContainerEl.style.maxHeight = `${window.innerHeight - listOffsetTop - 35}px`;
};
PubSub.subscribe(Events.WINDOW_RESIZE, handleWindowResize);
