import PubSub from 'pubsub-js';

import * as Utils from 'modules/Utils';
import * as Events from 'modules/Events';

import deptMapTpl from 'templates/deptMap.html';

/**
 * DeptMap module
 */

// Local variables
const VISIBLE_DISPLAY = 'flex';

let carte, carteId, btnId;

const tplData = {
  openLabel: 'Afficher/masquer la carte des départements',
  closeLabel: 'Fermer',
};

/**
 * Initialize module
 */
async function init() {
  await import('style/dept-map.scss');

  const container = Utils.tplToDom(deptMapTpl, tplData);

  // Add button
  const btn = container.querySelector('#dept-map-btn');
  btn.addEventListener('click', toggle);
  btnId = btn.id;
  Utils.appendAndShowModule(btn);

  // Add carte
  carte = container.querySelector('#dept-map');
  carteId = carte.id;
  const closeBtn = carte.querySelector('.popup-close');
  closeBtn.addEventListener('click', close);
  Utils.appendModule(carte);
}

/**
 * Toggle department map
 */
const toggle = () => {
  if (carte.style.display === VISIBLE_DISPLAY) {
    close();
  } else {
    open();
  }
};

/**
 * Open department map
 */
const open = () => {
  carte.style.display = VISIBLE_DISPLAY;
};

/**
 * Close department-map
 */
const close = () => {
  carte.style.display = 'none';
};

/**
 * Unfocus input when user click outside of it
 * @param event
 * @param target
 */
const checkClickedOut = (event, target) => {
  if (
    target.id !== btnId &&
    target.id !== carteId &&
    Utils.findParentNode(target, el => el.id === carteId) === null
  ) {
    close();
  }
};
PubSub.subscribe(Events.BODY_CLICKED, checkClickedOut);

Utils.executeOnDomReady(init);
