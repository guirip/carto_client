
// FIXME MODULE NON MAINTENU

/*
import PubSub from 'pubsub-js';
// import Pica from 'pica';

import * as BroadcastHelper from 'modules/BroadcastHelper';
import * as Context from 'modules/Context';
import * as ServiceHelper from 'modules/ServiceHelper';
import * as Utils from 'modules/Utils';

import uploadTpl from 'templates/upload.html';

import 'style/upload.scss';

/**
 * Upload module
 *
const
  uploadSuccessClass = 'upload-success',
  uploadFailureClass = 'upload-failure',
  regExpFilename = /([^\\/]*\.\w*)$/;

let $container,
  $commentaire,
  failuresCount,
  tplData = {
    closeLabel: 'Fermer',
    photosLabel: 'Photos à envoyer',
    inputLabel: 'Choisir des photos',
    commentaireLabel: 'Commentaire',
    validerLabel: 'Valider',
    validerTitle: 'Procéder au chargement des photos',
  };

export const events = {
  SHOW: 'UPL_SHOW',
};

/**
 * Show popup
 *
const show = () => {
  if (!$container) {
    $container = $(uploadTpl(tplData));
    $container.find('.popup-close').addEventListener('click', hide);
    $container.find('.btn-valider').addEventListener('click', performUpload);

    $commentaire = $container.find('.upload-commentaire > input');

    Utils.appendAndShowModule($container);
  }
  Utils.showPopup($container);
};
PubSub.subscribe(events.SHOW, show);

/**
 * Hide popup
 *
const hide = () => {
  Utils.hidePopup($container, resetForm);
};

/**
 * Reset form
 *
const resetForm = () => {
  // TODO
};

/**
 * Handle click on select file label
 *
const labelClickedHandler = function () {
  $(this).siblings('input').addEventListener('click', );
};

/**
 * Handle file selected event
 *
const fileSelectedHandler = function () {
  let matched = regExpFilename.exec(this.value);
  $(this)
    .siblings('.input-value')
    .text(matched ? matched[matched.length - 1] : this.value);
};

/**
 * Proceed to file upload
 *
const performUpload = () => {
  // TODO: disable submit button during network query

  let commentaire = $commentaire.val();
  failuresCount = 0;
  recursiveUpload($forms.toArray(), commentaire);
};

/**
 * Perform a single recursive upload
 * @param {array of elements} forms
 * @param {string} commentaire
 *
async function recursiveUpload(forms, commentaire) {
  let form = forms.shift();
  form.getElementsByClassName('upload-commentaire')[0].value = commentaire;

  // TODO

  await ServiceHelper.uploadPhoto(
    new FormData(form),
    Context.getCurrentFicheId(),

    // Success
    function (dto) {
      addClass(form, uploadSuccessClass);
      BroadcastHelper.broadcastPhotoAdded(dto, Context.getAuthor());

      proceedNext(forms, commentaire);
    },
    // Failure
    function (jqXhr) {
      addClass(form, uploadFailureClass);
      ServiceHelper.displayRequestError(
        jqXhr,
        form.getElementsByClassName('input-value')[0].textContent
      );
      failuresCount++;

      proceedNext(forms, commentaire);
    }
  );
};

/**
 * Add class to form
 * @param {element} form
 * @param {string}  cssClass
 *
const addClass = (form, cssClass) => {
  form.getElementsByClassName('input-label')[0].classList.add(cssClass);
};

/**
 * @param {array}  forms
 * @param {string} commentaire
 *
const proceedNext = (forms, commentaire) => {
  if (forms.length) {
    recursiveUpload(forms, commentaire);
  } else if (!failuresCount) {
    hide();
  }
};
*/
