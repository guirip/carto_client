import PubSub from 'pubsub-js';

import config from 'config';

import * as BroadcastHelper from 'modules/BroadcastHelper';
import * as Context from 'modules/Context';
import * as Events from 'modules/Events';
import * as ServiceHelper from 'modules/ServiceHelper';
import * as Utils from 'modules/Utils';

import * as Diapo from './Diapo';
import * as EtatsList from './EtatsList';
import * as Messages from './Messages';
import * as SaisieRemarque from './SaisieRemarque';
import * as SiteLink from './SiteLink';
import * as Upload from './Upload';

import etatTpl from 'templates/etat.html';

/**
 * Fiche module
 */

// Local variables
const ficheOffsetTop = 60;

const FOLD_TOGGLE_CLASS = 'fiche-folded';

let ficheContainer,
  fields,
  resizeTimeout,
  remarquesFetched = false,
  photosFetched = false;

const brgmFicheUrl =
  'http://' +
  String.fromCharCode(98) +
  String.fromCharCode(97) +
  String.fromCharCode(115) +
  String.fromCharCode(105) +
  String.fromCharCode(97) +
  String.fromCharCode(115) +
  String.fromCharCode(46) +
  String.fromCharCode(98) +
  String.fromCharCode(114) +
  String.fromCharCode(103) +
  String.fromCharCode(109) +
  '.fr/fiche_detaillee.asp?' +
  String.fromCharCode(73) +
  String.fromCharCode(68) +
  String.fromCharCode(84) +
  String.fromCharCode(61);

export const events = {
  OPEN: 'FIC_OPEN',
  CLOSE: 'FIC_CLOSE',
};

/**
 * @param value
 */
const getOuiNonLibelle = (value) => {
  switch (parseInt(value, 10)) {
    case 0:
      return 'Non';
    case 1:
      return 'Oui';
    case 2:
      return 'Partiellement';
    default:
      return '?';
  }
};

const isPhotoFeatureEnabled = config.photo && config.photo.featureEnabled;

/**
 * Template data for a fiche
 */
const tplFicheData = {
  foldLabel: 'Agrandir/réduire la fiche',
  zoomLabel: 'Zoom sur ce lieu',
  linkLabel: 'Partager ce lieu avec les copains',
  brgmLabel: 'Voir la fiche détaillée sur le site du BASIAS',
  prevLabel: 'Site précédent',
  nextLabel: 'Site suivant',
  closeLabel: 'Fermer la fiche',

  identifiantLabel: 'Identifiant',
  raisonSocialeLabel: 'Raison sociale',
  nomUsuelLabel: 'Nom usuel',

  etatLabel: 'Etat',
  etatBtnTitle: "Changer l'état",

  remarquesLabel: 'Commentaires',
  ajouterRemarqueLabel: 'Ecrire un commentaire',
  remarqueBtnTitle: 'Ajouter un commentaire sur ce lieu',

  photosLabel: 'Photos',
  photosBtnTitle:
    'Envoyer des photos sur le site pour les joindre à cette fiche',
  ajouterPhotosLabel: 'Ajouter des photos',

  isPhotoFeatureEnabled,
  staticPathThumbs: config.staticPath + config.photoDir + config.staticPathThumbs,

  commentaireLabel: 'Commentaire',
  adresseLabel: 'Adresse',
  activiteLabel: 'Activité',
  commentaireLocalisationLabel: 'Commentaire localisation',
  etatDeConnaissanceLabel: 'Etat de connaissance',
  etatOccupationLabel: 'Etat occupation',
  siteReamenageLabel: 'Site réaménage',
  siteEnFricheLabel: 'Site en friche',
  typeDeReamenagementLabel: 'Type de réaménagement',
  projetDeReamenagementLabel: 'Projet de réaménagement',
  coordonnesWgsLabel: 'Coordonnées',
};

/**
 * Initialize module
 */
async function init() {
  await import('style/fiche.scss');

  ficheContainer = document.getElementById('fiche-container');

  // Work-around to keep display: inline-block
  ficheContainer.style.display = 'none';
  ficheContainer.style.opacity = 1;
}

/**
 * Open fiche
 * @param event
 * @param site
 */
async function openFiche(event, site) {
  tplFicheData.site = site;

  tplFicheData.siteEnFricheLibelle = getOuiNonLibelle(site.SITE_EN_FRICHE);
  tplFicheData.siteReamenageLibelle = getOuiNonLibelle(site.SITE_REAMENAGE);
  tplFicheData.etatLibelle = EtatsList.getEtat(site.ETAT).libelle;
  tplFicheData.etatAuthor = site.ETAT_AUTHOR && site.ETAT_AUTHOR != 'IMPORT' ? site.ETAT_AUTHOR : null;
  tplFicheData.creationDate = Utils.formatDate(site.CREATION_DATE);
  tplFicheData.etatDate = Utils.formatDate(site.ETAT_DATE);
  tplFicheData.positionDate = Utils.formatDate(site.POSITION_DATE);
  tplFicheData.htmlEtat = etatTpl(tplFicheData);
  tplFicheData.coordinates = `${site.GEO_POINT.coordinates[1]}, ${site.GEO_POINT.coordinates[0]}`;
  tplFicheData.hasClipboardApi = !!navigator.clipboard;

  remarquesFetched = false;

  const { data:remarques } = await ServiceHelper.getRemarques(site._id);
  if (Array.isArray(remarques) !== true) {
    remarques = [];
  }
  tplFicheData.hasRemarques = remarques.length > 0;
  remarques.forEach((remarque) => {
    remarque.dateLabel = Utils.formatDate(remarque.DATE);
  });
  tplFicheData.remarques = remarques;
  remarquesFetched = true;
  await applyTemplate();

  photosFetched = false;
  if (config.photo.featureEnabled) {
    let { data:photos } = await ServiceHelper.getPhotos(site._id);
    if (Array.isArray(photos) !== true) {
      photos = [];
    }
    tplFicheData.hasPhotos = photos.length > 0;
    tplFicheData.photos = photos;
    photosFetched = true;
    await applyTemplate();
  }
};
PubSub.subscribe(events.OPEN, openFiche);

/**
 * Apply template to generate full fiche html
 */
const applyTemplate = async () => {
  if (remarquesFetched && (photosFetched || !config.photo.featureEnabled)) {
    const { default: ficheTpl } = await import('templates/fiche.html');
    ficheContainer.innerHTML = ficheTpl(tplFicheData, 1);
    setEventListeners();
    fields = Utils.findChildren(ficheContainer, c => c.classList.contains('fiche-fields'))[0];
    performResize();
    Utils.showPopup(ficheContainer);
  }
};

function setEventListeners() {
  [
    { selector: '.fiche-fold-btn', handler: foldToggle },
    { selector: '.fiche-btn-zoom', handler: zoomOnCurrentSite },
    { selector: '.fiche-btn-link', handler: displayLinkToSite },
    { selector: '.fiche-btn-brgm', handler: openBrgmFiche, mandatory: false },
    { selector: '.fiche-btn-prev', handler: showPreviousSite },
    { selector: '.fiche-btn-next', handler: showNextSite },
    { selector: '#fiche-move-site-btn', handler: enableSiteMove },
    { selector: '.popup-close', handler: broadcastCloseFicheEvent },
    { selector: '.current-etat-container', handler: displayEtatsList },
    { selector: '.btn-ajouter-remarque', handler: addRemarque },
    isPhotoFeatureEnabled
      ? { selector: '.btn-ajouter-photos', handler: addPhotos }
      : null,
    isPhotoFeatureEnabled
      ? { selector: '.fiche-photo', handler: openPhotos }
      : null,
    { selector: '.fiche-coordinates button', handler: copyCoordinates },
  ]
  .filter(i => i)
  .forEach(({ selector, handler, mandatory }) => {
    const el = ficheContainer.querySelector(selector);
    if (!el) {
      if (mandatory !== false) {
        console.warn('could not match selector: '+selector);
      }
      return;
    }
    el.addEventListener('click', handler);
  });
}

/**
 * Close fiche
 */
const closeFiche = () => {
  Utils.hidePopup(ficheContainer);
};
PubSub.subscribe(events.CLOSE, closeFiche);

/**
 * Broadcast an event to all module to update context
 */
const broadcastCloseFicheEvent = () => {
  PubSub.publish(events.CLOSE);
};

const zoomOnCurrentSite = () => {
  PubSub.publish(Events.SITE_ZOOM_CURRENT);

  if (Utils.isMobileDevice()) {
    broadcastCloseFicheEvent();
  }
};

/**
 * Ability to reduce UI size, or display everything
 */
function foldToggle() {
  Utils.toggleCssClass(ficheContainer, FOLD_TOGGLE_CLASS);
}

/**
 * Called when link icon is clicked
 * @param e
 */
const displayLinkToSite = (e) => {
  if (Context.getCurrentFicheId()) {
    const link = SiteLink.linkOnSite(Context.getCurrentFicheId());

    if (link && navigator.clipboard?.writeText) {
      navigator.clipboard.writeText(link);
      Messages.info('Lien copié 👍');
    }
  }
  e.stopPropagation();
};

/**
 * @param e
 */
const displayEtatsList = function (e) {
  PubSub.publish(EtatsList.events.OPEN, {
    etat: e.target.dataset.id,
    callback: ficheEtatSelected,
  });
  e.stopPropagation();
};

/**
 * Perform site etat update
 * @param id
 */
async function ficheEtatSelected(id) {
  const { data:updatedSite, error } = await ServiceHelper.saveEtat(Context.getCurrentFicheId(), id);
  if (error) return;

  if (!updatedSite.SITE_ID) {
    updatedSite.SITE_ID = updatedSite._id;
  }

  BroadcastHelper.broadcastSiteEtatUpdated(updatedSite, Context.getAuthor());
}

const addRemarque = () => {
  PubSub.publish(SaisieRemarque.events.SHOW);
};

const addPhotos = () => {
  PubSub.publish(Upload.events.SHOW);
};

/**
 * Send an event to display diaporama
 */
const openPhotos = function (e) {
  PubSub.publish(Diapo.events.SHOW, {
    siteId: Context.getCurrentFicheId(),
    photoId: e.target.dataset.id,
    title: tplFicheData.site.NOM_USUEL || tplFicheData.site.RAISON_SOCIALE,
  });
};

/**
 * Copy the current site's coordinates in user's clipboard
 * @param  {object} event
 */
function copyCoordinates({target}) {
  navigator.clipboard.writeText(target.dataset.coordinates);
  Messages.info('Coordonnées copiées 👍');
}

/**
 * Called when brgm icon is clicked
 * @param e
 */
const openBrgmFiche = (e) => {
  const popup = window.open(
    brgmFicheUrl + e.target.attributes['data-identifiant'].value,
    '_blank'
  );
  if (!popup) {
    Messages.warn('popup blocked');
  }
  e.stopPropagation();
};

function showPreviousSite() {
  PubSub.publish(Events.MARKER_GO_TO_PREVIOUS, tplFicheData.site._id);
}
function showNextSite() {
  PubSub.publish(Events.MARKER_GO_TO_NEXT, tplFicheData.site._id);
}

function enableSiteMove() {
  // if (Utils.isMobileDevice()) {
  closeFiche();
  // }
  PubSub.publish(Events.MARKER_DRAGGABLE, {
    draggable: true,
    siteId: Context.getCurrentFicheId(),
  });
}

const handleWindowResize = () => {
  if (resizeTimeout) {
    clearTimeout(resizeTimeout);
  }
  resizeTimeout = setTimeout(performResize, 100);
};
const performResize = () => {
  if (fields) {
    fields.style.maxHeight = `${window.innerHeight - ficheOffsetTop - 40}px`;
  }
};
PubSub.subscribe(Events.WINDOW_RESIZE, handleWindowResize);

Utils.executeOnDomReady(init);
