import iconFactory0 from 'assets/img/factory.0.png';
import iconFactory1 from 'assets/img/factory.1.png';
import iconFactory2 from 'assets/img/factory.2.png';
import iconFactory3 from 'assets/img/factory.3.png';
import iconFactory4 from 'assets/img/factory.4.png';
import iconFactory5 from 'assets/img/factory.5.png';
import iconFactory6 from 'assets/img/factory.6.png';
import iconFactory7 from 'assets/img/factory.7.png';
import iconFactory8 from 'assets/img/factory.8.png';
import iconFactory9 from 'assets/img/factory.9.png';
import iconFactory10 from 'assets/img/factory.10.png';
import iconFactory11 from 'assets/img/factory.11.png';
import iconFactory12 from 'assets/img/factory.12.png';

import selectedIconFactory0 from 'assets/img/selected.factory.0.png';
import selectedIconFactory1 from 'assets/img/selected.factory.1.png';
import selectedIconFactory2 from 'assets/img/selected.factory.2.png';
import selectedIconFactory3 from 'assets/img/selected.factory.3.png';
import selectedIconFactory4 from 'assets/img/selected.factory.4.png';
import selectedIconFactory5 from 'assets/img/selected.factory.5.png';
import selectedIconFactory6 from 'assets/img/selected.factory.6.png';
import selectedIconFactory7 from 'assets/img/selected.factory.7.png';
import selectedIconFactory8 from 'assets/img/selected.factory.8.png';
import selectedIconFactory9 from 'assets/img/selected.factory.9.png';
import selectedIconFactory10 from 'assets/img/selected.factory.10.png';
import selectedIconFactory11 from 'assets/img/selected.factory.11.png';
import selectedIconFactory12 from 'assets/img/selected.factory.12.png';

import _userIcon from 'assets/img/user-position.png';

export const iconsPerEtat = {
  0: iconFactory0,
  1: iconFactory1,
  2: iconFactory2,
  3: iconFactory3,
  4: iconFactory4,
  5: iconFactory5,
  6: iconFactory6,
  7: iconFactory7,
  8: iconFactory8,
  9: iconFactory9,
  10: iconFactory10,
  11: iconFactory11,
  12: iconFactory12,
};

export const selectedIconsPerEtat = {
  0: selectedIconFactory0,
  1: selectedIconFactory1,
  2: selectedIconFactory2,
  3: selectedIconFactory3,
  4: selectedIconFactory4,
  5: selectedIconFactory5,
  6: selectedIconFactory6,
  7: selectedIconFactory7,
  8: selectedIconFactory8,
  9: selectedIconFactory9,
  10: selectedIconFactory10,
  11: selectedIconFactory11,
  12: selectedIconFactory12,
};

export const userIcon = _userIcon;

export const getUserIconWithCourse = rotate => (
  `<svg xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:cc="http://creativecommons.org/ns#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd" xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape" version="1.1" id="svg2" viewBox="0 0 60 60" height="60" width="60" inkscape:version="0.91 r13725" sodipodi:docname="user-position-with-course.svg" transform="rotate(${rotate})">
    <sodipodi:namedview pagecolor="#ffffff" bordercolor="#666666" borderopacity="1" objecttolerance="10" gridtolerance="10" guidetolerance="10" inkscape:pageopacity="0" inkscape:pageshadow="2" inkscape:window-width="1920" inkscape:window-height="1031" id="namedview10" showgrid="false" inkscape:zoom="22.250293" inkscape:cx="23.232111" inkscape:cy="34.586417" inkscape:window-x="0" inkscape:window-y="1107" inkscape:window-maximized="1" inkscape:current-layer="svg2"/>
    <defs id="defs4"/>
    <metadata id="metadata7">
      <rdf:RDF>
        <cc:Work rdf:about="">
          <dc:format>image/svg+xml</dc:format>
          <dc:type rdf:resource="http://purl.org/dc/dcmitype/StillImage"/>
          <dc:title/>
        </cc:Work>
      </rdf:RDF>
    </metadata>
    <path style="fill:#0044aa;fill-rule:evenodd;stroke:#ffffff;stroke-width:2;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" d="m 30,4.62 l -8,17.117 l 16,0 z" id="path4236"/>
    <path style="opacity:1;fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:0.1;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" d="m 43.163,30.049 a 13.164,13.164 0 0 1 -13.164,13.2 a 13.164,13.164 0 0 1 -13.164,-13.2 a 13.164,13.164 0 0 1 13.164,-13.2 a 13.164,13.164 0 0 1 13.164,13.2 z" id="path4138"/>
    <circle style="opacity:1;fill:#0044aa;fill-opacity:1" id="path4138-0" cx="29.999" cy="30.049" r="11.118"/>
    <circle style="opacity:1;fill:#ffffff;fill-opacity:1" id="path4138-0-3" cx="29.999" cy="30.049" r="6.8417"/>
    <circle style="opacity:1;fill:#0055d4;fill-opacity:1" id="path4138-0-3-0" cx="10.981044" cy="40.981163" r="4.5377007" transform="matrix(0.86602655,-0.49999801,0.49999801,0.86602655,0,0)"/>
  </svg>`
);
