import PubSub from 'pubsub-js';

import * as BroadcastHelper from 'modules/BroadcastHelper';
import * as Utils from 'modules/Utils';
import * as Events from 'modules/Events';
import * as Context from 'modules/Context';
import * as ServiceHelper from 'modules/ServiceHelper';
import * as GeocoderHelper from 'modules/GeocoderHelper';
import * as Messages from 'ui/Messages';
import { events as FicheEvents } from 'ui/Fiche';

import dragInfoTpl from 'templates/dragInfo.html';

/**
 * DragHandler module
 */

// Local variables
let dragInfoContainer, currentMarker;

/**
 * Drag info template data
 */
const tplDragInfoData = {
  titre: 'Nouvelle position',
  adresseLabel: 'Adresse',
  coordonnesWgsLabel: 'Coordonnées',
  validerLabel: 'Valider',
  annulerLabel: 'Annuler',
};

/**
 * Initialize module
 */
async function init() {
  await import('style/drag-handler.scss');

  dragInfoContainer = document.getElementById('drag-info-container');

  // Work-around to keep display: inline-block
  dragInfoContainer.style.display = 'none';
  dragInfoContainer.style.opacity = 1;
}

/**
 * Save marker's new position
 */
async function validateDrag({ target }) {
  const { data:updatedSite, error } = await ServiceHelper.savePosition(
    Context.getCurrentDragId(),
    target.dataset.x,
    target.dataset.y,
    target.dataset.adresse,
    target.dataset.commune,
    target.dataset.dept,
    target.dataset.pays,
  );
  if (error) return;

  if (!updatedSite.SITE_ID) {
    updatedSite.SITE_ID = updatedSite._id;
  }

  BroadcastHelper.broadcastSitePositionUpdated(updatedSite, Context.getAuthor());
  Messages.info('La nouvelle position a bien été enregistrée.');
  end();
}

/**
 * Cancel marker drag
 */
export const cancelDrag = () => {
  if (!currentMarker) {
    // Something went wrong
    console.error('Cannot cancel drag because currentMarker is not set');
    return;
  }

  PubSub.publish(Events.MARKER_MOVE, {
    id: Context.getCurrentDragId(),
    position: currentMarker.positionBeforeDrag,
  });
  end();
};

/**
 * Close the popup, reset state
 */
function end() {
  PubSub.publish(Events.MARKER_DRAGGABLE, {
    draggable: false,
    siteId: Context.getCurrentDragId(),
  });
  PubSub.publish(FicheEvents.CLOSE);

  Utils.hidePopup(dragInfoContainer);
  Context.setCurrentDragId('');

  currentMarker.positionBeforeDrag = null;
  currentMarker = null;
}

/**
 * Called every time the marker starts being dragged
 */
export const markerDragstartHandler = function () {
  // Si un autre marker a été déplacé (sans confirmation ni annulation), annulation du déplacement
  if (Context.getCurrentDragId() && Context.getCurrentDragId() != this.id) {
    cancelDrag();
  }

  if (!currentMarker) {
    // First drag
    Context.setCurrentDragId(this.id);
    currentMarker = this;
    currentMarker.positionBeforeDrag = this.position;
  }
};

/**
 * Called when marker is dropped
 * @param e
 */
export const markerDragendHandler = async ({latLng}) => {
  const result = await GeocoderHelper.performGeocode({
    latLng,
  });
  if (result) {
    markerDragedGeocoded(
      latLng,
      GeocoderHelper.getAddressObject(result)
    );
  }
};

/**
 * Called when dragged marker new position has been geocoded
 * @param data
 */
const markerDragedGeocoded = (latLng, address) => {
  tplDragInfoData.x = Utils.roundWgs(latLng.lng());
  tplDragInfoData.y = Utils.roundWgs(latLng.lat());
  tplDragInfoData.adresse = address.adresse;
  tplDragInfoData.commune = address.commune;
  tplDragInfoData.dept = address.dept;
  tplDragInfoData.pays = address.pays;

  dragInfoContainer.innerHTML = dragInfoTpl(tplDragInfoData, 1);
  setEventListeners();
  Utils.showPopup(dragInfoContainer);
};

function setEventListeners() {
  dragInfoContainer.querySelector('.ok').addEventListener('click', validateDrag);
  dragInfoContainer.querySelector('.ko').addEventListener('click', cancelDrag);
}

Utils.executeOnDomReady(init);
