import PubSub from 'pubsub-js';

import * as BroadcastHelper from 'modules/BroadcastHelper';
import * as Context from 'modules/Context';
import * as Events from 'modules/Events';
import * as GeocoderHelper from 'modules/GeocoderHelper';
import * as ServiceHelper from 'modules/ServiceHelper';
import * as Utils from 'modules/Utils';
import * as Messages from 'ui/Messages';

import * as EtatsList from './EtatsList';

import etatTpl from 'templates/etat.html';
import creationSiteTpl from 'templates/creationSite.html';

import 'style/site-creation.scss';
import 'style/infowindow.override.scss';

let
  formIB,
  siteCreationFormEl;

const getEtatLibelle = (etat) => {
  return EtatsList.getEtat(etat).libelle;
};

/**
 * Creation site template data
 */
const DEFAULT_ETAT = 7;
const tplSiteCreationData = {
  closeLabel: 'Annuler',
  nomLabel: 'Nom',
  etatLabel: 'Etat',
  etatBtnTitle: "Changer l'état",
  showEtatAuthor: false,
  site: {
    ETAT: DEFAULT_ETAT,
  },
  validerLabel: 'Créer',
  validerTitle: 'Valider la création du lieu',
};

EtatsList.doWhenReady(() => {
  tplSiteCreationData.etatLibelle = getEtatLibelle(tplSiteCreationData.site.ETAT);
  tplSiteCreationData.htmlEtat = etatTpl(tplSiteCreationData);
});

/**
 * @param e
 */
const displayEtatsList = function(e) {
  PubSub.publish(EtatsList.events.OPEN, {
    etat: e.target.dataset.id,
    callback: etatSelected,
  });
  e.stopPropagation();
};

/**
 * @param etat
 */
const etatSelected = (etat) => {
  const divTmp = document.createElement('div');
  divTmp.innerHTML = etatTpl({
    site: { ETAT: etat },
    etatLibelle: getEtatLibelle(etat),
  });
  const newEl = divTmp.querySelector('.current-etat-container');
  newEl.addEventListener('click', displayEtatsList);

  siteCreationFormEl.querySelector('.current-etat-container').replaceWith(newEl);
};

/**
 * @param e
 */
const validateForm = async (e) => {
  e.stopPropagation();

  const position = formIB.getPosition();
  const {address} = formIB;

  const submitButton = e.target;
  submitButton.disabled = true;
  try {
    await performSiteCreation({
      nom: siteCreationFormEl.querySelector('.input-nom').value,
      etat: siteCreationFormEl.querySelector('.current-etat-container').dataset.id,
      ...address,
      longitude: Utils.roundWgs(position.lng()),
      latitude: Utils.roundWgs(position.lat()),
    });
    formIB.close();
  } catch (e) {
    Messages.error('Flûte, erreur lors de l\'ajout du site 😞', e.message);
  }
  submitButton.disabled = false;
};

/**
 * calls backend to persist the new site
 * @param data
 */
async function performSiteCreation(data) {
  const { createdSite, error } = await ServiceHelper.createSite(data);
  if (error) return;

  if (!createdSite.SITE_ID) {
    createdSite.SITE_ID = createdSite._id;
  }
  BroadcastHelper.broadcastSiteCreated(createdSite, Context.getAuthor());
}

/**
 * Show form
 * @param  {string} _eventName
 * @param  {LatLng} position
 */
async function show(_eventName, position) {
  PubSub.publish(Events.MAP_SHOW_TOOLTIP, {
    ttp: formIB,
    position
  });

  const result = await GeocoderHelper.performGeocode({
    latLng: position,
  });
  if (!result) {
    hide();
    return
  }
  const address = GeocoderHelper.getAddressObject(result);
  formIB.address = address;
}
PubSub.subscribe(Events.SITE_CREATION_SHOW, show);

/**
 * Hide form
 * @param e
 */
const hide = (e) => {
  e?.stopPropagation();
  formIB.close();

  PubSub.publish(EtatsList.events.CLOSE);
};

/**
 * Initialize the form displayed to create a site
 */
function initSiteCreationForm() {
  formIB = new google.maps.InfoWindow({
    zIndex: 2,
    content: creationSiteTpl(tplSiteCreationData, true),
  });

  formIB.addListener('domready', function () {
    siteCreationFormEl = document.getElementById('creation-site-container');
    Utils.overrideInfowindowCloseIcon(siteCreationFormEl);

    // Add event handlers
    [
      ['.popup-close', hide],
      ['.current-etat-container', displayEtatsList],
      ['.btn-valider', validateForm],
    ]
    .forEach(([ selector, handler ]) => {
      const el = siteCreationFormEl.querySelector(selector);
      if (!el) {
        console.warn('[site creation form] cannot match selector: '+selector);
        return;
      }
      siteCreationFormEl.querySelector(selector).addEventListener('click', handler);
    });

    siteCreationFormEl.querySelector('.input-nom').value = 'Créé par '.concat(Context.getAuthor());
  });
}

const init = function () {
  if (!EtatsList.isReady()) {
    EtatsList.doWhenReady(initMenu);
  } else {
    initSiteCreationForm();
  }
};

init();