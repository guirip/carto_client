import PubSub from 'pubsub-js';

import * as Events from 'modules/Events';

import 'style/waiter.scss';

/**
 * Waiter module
 */

/**
 * Return the DOM element (fetch it first if needed)
 * @return {DOM element}
 */
const getContainerEl = (() => {
  let el;

  return () => {
    if (typeof el === 'undefined') {
      el = document.querySelector('#waiter');
    }
    return el;
  };
})();

/**
 * Show container
 */
export const show = () => {
  getContainerEl().style.display = 'block';
};

/**
 * Hide container
 */
const hide = () => {
  reset();
};
PubSub.subscribe(Events.AUTH_SUCCESS, hide);

/**
 * Reset container style
 */
const reset = () => {
  getContainerEl().style.display = 'none';
  getContainerEl().style.opacity = 1;
};
