import { executeOnDomReady } from './modules/Utils';
import { init } from './views/Map';

console.info('About to init map view');

executeOnDomReady(init);
