import { executeOnDomReady } from './modules/Utils';
import { init } from './views/Login';

console.info('About to init login view');

executeOnDomReady(init);
