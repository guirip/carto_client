
# TODOS

- MIGRATION: google marker api
- FIX: sites non cliquables sur les Etats-Unis

- LATER: supprimer les champs X_WGS_84 et Y_WGS_84

- partager une adresse vers l'application plan du téléphone ?
  - piste : <https://developer.mozilla.org/en-US/docs/Web/API/Navigator/share>
  - si je partage des coordonnées peut-être que l'OS peut proposer d'ouvrir ça avec une appli plan

- Photos
  - backend
  - fix debounce in Map.js
  - ajout de ces évènements au journal

- CSS transitions wherever possible (translate, opacity)

- Ne pas attendre l'évènement websocket pour supprimer le tooltip local precedent

- Ne pas attendre l'évènement websocket pour le changement d'état d'un site ?

- saisie-remarque : clic externe
  - si champ vide: fermeture
  - sinon demander confirmation ?

# Version 1.1.1 - Mise en prod le 24/08/2024

- Màj des dépendances, notamment migration CKeditor (fin de support V4)
- Back:
  - Migration des callbacks restants vers async/await

# Version 1.1.0 - Mise en prod le 30/10/2021

- Prise en charge du numéro de département en tant que string (Corse, étranger)
- Amélioration du traitement de la réponse du géocodage Google afin de mieux supporter l'absence de certaines infos (cp, pays, etc)
- Ajout de données pour la Corse (2A, 2B) et le Nord (59)
- Correction: màj de l'adresse lors du déplacement d'un site
- Fiche:
  - possibilité de naviguer vers le site suivant/précédent
  - possibilité de réduire la fiche (sur mobile seulement)
- Géolocalisation: support de l'orientation, mais désactivé car fréquence de màj insuffisante
- Améliorations et corrections mineures (bundle size, etc)

# Version 1.0.2 - Mise en prod le 25/10/2021

- Possibilité d'afficher les sites autour d'une position (geo json)
- Copie automatique dans le presse-papier du lien direct vers un site
- Possibilité de copier dans le presse-papier les coordonnées géographique d'un site
- Corrections et ajustements mineurs
- Partie technique:
  - Front:
    - suppression de jquery
    - migration webpack
    - màj dépendances
    - amélioration de la fonctionnalité de géolocalisation
  - Back:
    - babel build
    - màj dépendances
    - migration champs X/Y_WGS_84 (format texte) vers le champ GEO_POINT au format GeoJSON

# Version 1.0.1 - Mise en prod le 15/09/2020

- Bouton GPS:
  - Attente d'une position ➡ le bouton clignotte
  - Etat actif ➡ couleur verte + mise à jour de la position à interval régulier (3,5 sec)
  - Nouvel appui sur le bouton ➡ arrêt de la géolocalisation

# Version 1.0 - Mise en prod le 14/08/2020

- Connexion sécurisée (https, wss)

- Upgrade de l'interface
  - version mobile
  - amélioration de l'ergonomie pour le déplacement de sites
  - ajout des états: "Royaume démoli" et "A surveiller"
  - affichage des sites hors France
  - possibilité de se géolocaliser (point de départ potentiel pour des features à venir)
  - les modifications d'état et ajouts de sites tiennent compte du filtre courant
  - améliorations et corrections mineures diverses

- Remplacement du process de build de la partie cliente (gulp+requireJS -> webpack)

- Réécriture complète de la partie serveur (php+mysql -> node.js+mongo)
  - réécriture des webservices
    - DIAG     : ✓
    - LOGIN    : ✓
    - SITES    : ✓
    - COMMUNES : ✓
    - ETATS    : ✓
    - REMARQUES: ✓
    - JOURNAL  : ✓
    - PHOTOS   : TODO
  - export mysql :
    - SITES    : ✓
    - COMMUNES : ✓
    - ETATS    : ✓
    - REMARQUES: ✓
    - PHOTOS   : ✓

- Nouveau serveur dédié (heytonton2)

# Version 0.2.1 - Mise en prod le 21/10/2015

- Ajustements suite au nouveau layout de google maps

- Services d'export de la base de données (pour migration php/mysql->node/mongodb)

# Version 0.2.0 - Mise en prod le 10/07/2015

- Page d'aide

- Notes de versions

- Bugfixes
  - Journal: Le filtre ne s'appliquait pas sur les évènements provenant du temps reel
  - Journal: Ajout d'une remarque en temps reel = le numero de departement n'etait pas indique
  - Correction du bug a l'annulation du deplacement en cas de focus sur un autre site
  - Recherche: Correction de la regexp pour les coordonnees geodesique

- Journal: Lors de l'ajout d'un lieu, montrer le nom plutot que l'adresse
- Recherche: Support des coordonnees sexagesimal (du type: 48&deg;49'53.36"N 0&deg;29'38.70"E)

# Version 0.1 - Mise en prod le 23/06/2015

## Front

- Login

- Ecran de chargement

- Récuperation des sites
  - Tous
  - Par départements
  - Par id

- Création d'un site
  - Normalisation de la commune pour la recherche du code insee
  - Choix du nom
  - Choix de l'état

- Fiche info
  - Zoom sur site
  - Icone link pour partager un lien direct vers un site
  - Lien vers la fiche complète sur le site source
  - Afficher l'auteur et date de mise à jour de l'état si != import
  - Afficher les remarques

- Configurer CKEditor pour la saisie de remarques

- Mise à jour d'un site
  - Etat
  - Position
  - Ecrire une remarque

- Street view minimap

- Drag/drop site pour corriger la position
  - Visualisation de la reverse address
  - Possibilité d'enregistrer ou d'annuler le déplacement

- Rond différenciant le site sélectionné

- Push
  - Position
  - Etat
  - Tooltip indiquant qui est sur quelle fiche
    - gestion de plusieurs utilisateurs sur un même site
    - fermeture tooltip lors de la fermeture de la fenêtre

- Champ de recherche par adresse
  - Support de coordonnées au format géodésique (du type: 48.831489, -0.494083)

- Refactoring :
  - Extraire autant de modules que possible
  - Extraire les templates, les charger par le module correspondant (idem pour le css)

- Journal d'évènements
  - Possibilite de le plier/deplier
  - Lorsqu'il est plié, compteur de nouveaux évènements
  - Remarques
  - Pagination
  - Filtre par types d'évènements

- Diag
  - DB
  - WS

- Filtres
  - par départements
  - par états

- Notifications

- Photos
  - Upload de photos
  - Gallerie de ces photos

- Déconnexion

## Back

- Batch java pour télécharger tous les fichiers CSV
- Alimentation base de données
- Géocodage WGS84

- URL Rewritting pour les appels REST
- Vérification session sur chaque requête serveur
- Check de sante (diag)

## Websocket

- Remplacement du serveur php par node+socket-io

# Démarrage du projet - 12/11/2014
